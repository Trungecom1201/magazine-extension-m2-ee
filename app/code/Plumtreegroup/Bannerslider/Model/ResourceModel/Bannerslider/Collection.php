<?php

namespace Plumtreegroup\Bannerslider\Model\ResourceModel\Bannerslider;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected $_previewFlag;

    protected $_idFieldName = 'id';

    public function _construct()
    {
        $this->_init('Plumtreegroup\Bannerslider\Model\Bannerslider', 'Plumtreegroup\Bannerslider\Model\ResourceModel\Bannerslider');
    }

}