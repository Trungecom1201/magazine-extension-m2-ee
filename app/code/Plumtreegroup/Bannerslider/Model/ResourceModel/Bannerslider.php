<?php

namespace Plumtreegroup\Bannerslider\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Bannerslider extends AbstractDb {

    protected function _construct()
    {
        $this->_init('banner_slider', 'id');
    }
}