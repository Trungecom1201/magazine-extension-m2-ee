<?php

namespace Plumtreegroup\Bannerslider\Model;

use Magento\Framework\Model\AbstractModel;

class Bannerslider extends AbstractModel
{

    protected function _construct()
    {
        $this->_init('Plumtreegroup\Bannerslider\Model\ResourceModel\Bannerslider');
    }

}