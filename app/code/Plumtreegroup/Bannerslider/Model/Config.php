<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Plumtreegroup\Bannerslider\Model;
class Config
{
    const XML_LIMIT_ITEM_BANNER_SLIDER = 'bannersection/general/limit';
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }
    public function getLimit(){
        return (int)$this->scopeConfig->getValue(self::XML_LIMIT_ITEM_BANNER_SLIDER);
    }
}