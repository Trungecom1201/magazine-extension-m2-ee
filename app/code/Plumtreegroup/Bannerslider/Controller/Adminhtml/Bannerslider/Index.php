<?php

namespace Plumtreegroup\Bannerslider\Controller\Adminhtml\Bannerslider;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

class Index extends Action
{

    protected $resultPageFactory;

    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Plumtreegroup_Bannerslider::bannerslider_content');
        $resultPage->addBreadcrumb(__('Banner slider'), __('Banner slider'));
        $resultPage->addBreadcrumb(__('Manage Banner slider'), __('Manage Banner slider'));
        $resultPage->getConfig()->getTitle()->prepend(__('Banner slider'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Plumtreegroup_Bannerslider::bannerslider_content');
    }
}