<?php

namespace
Plumtreegroup\Bannerslider\Setup;

use Magento\Backend\Block\Widget\Tab;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $pdfTable = $installer->getConnection()
            ->newTable($installer->getTable('banner_slider'))
            ->addColumn('id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true])

            ->addColumn('name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''])

            ->addColumn('image',
                Table::TYPE_TEXT,
                500,
                ['nullable' => false, 'default' => ''])

            ->addColumn('url',
                Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => ''])

            ->addColumn('description',
                Table::TYPE_TEXT,
                500,
                ['nullable' => true, 'default' => ''])
            
            ->addColumn('button',
                Table::TYPE_TEXT,
                500,
                ['nullable' => true, 'default' => ''])

            ->addColumn('sub_title',
                Table::TYPE_TEXT,
                500,
                ['nullable' => true, 'default' => ''])


            ->addColumn('order',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true, 'default' => 0])

            ->addColumn('status',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => true, 'default' => 0]);

        $installer->getConnection()->createTable($pdfTable);

        $installer->endSetup();
    }
}