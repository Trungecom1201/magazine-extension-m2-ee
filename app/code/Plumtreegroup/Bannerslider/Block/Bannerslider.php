<?php

namespace Plumtreegroup\Bannerslider\Block;



class Bannerslider extends \Magento\Framework\View\Element\Template {

    /**
     * @var \Plumtreegroup\Bannerslider\Model\ResourceModel\Bannerslider\CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;
    /**
     * @var \Plumtreegroup\Bannerslider\Model\Config
     */
    protected $config;

    /**
     * Bannerslider constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Plumtreegroup\Bannerslider\Model\ResourceModel\Bannerslider\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     * @param \Plumtreegroup\Bannerslider\Model\Config $config
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,

        \Plumtreegroup\Bannerslider\Model\ResourceModel\Bannerslider\CollectionFactory $collectionFactory,
        \Magento\Framework\Registry $registry,
        array $data = [],\Plumtreegroup\Bannerslider\Model\Config $config)
    {
        $this->_registry = $registry;
        $this->collectionFactory = $collectionFactory;
        $this->config = $config;
        parent::__construct($context, $data);
    }

    public function getCollections() {
        $limit = $this->config->getLimit();
        $Collection = $this->collectionFactory->create()
            ->addFieldToSelect(array('name','image','url','description','button'))
            ->addFieldToFilter('status',1);
        $Collection->getSelect()->limit($limit);
        return $Collection;
    }
}