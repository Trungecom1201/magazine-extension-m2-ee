<?php

namespace Plumtreegroup\Bannerslider\Block\Adminhtml\Bannerslider\Edit;

use Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic {

    protected $_systemStore;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('bannerslider_form');
        $this->setTitle(__('Banner Slider <span class="sp-highlight-term">Information</span>'));
    }

    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('bannerslider_data');

        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post', 'enctype' => 'multipart/form-data']]
        );

        $form->setHtmlIdPrefix('bannerslider_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General <span class="sp-highlight-term">Information</span>'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

        $fieldset->addField(
            'name',
            'text',
            ['name' => 'name', 'label' => __('Title'), 'title' => __('Title'), 'required' => true]
        );

        $fieldset->addField(
            'sub_title',
            'text',
            [
                'name' => 'sub_title',
                'label' => __('Sub Title'),
                'title' => __('Sub Title'),
                'required' => false,
            ]
        );
        $fieldset->addField(
            'image',
            'image',
            [
                'name' => 'image',
                'label' => __('Image'),
                'title' => __('Image'),
                'class' => 'required-entry',
                'required' => true,
                'note'      => '(*.jpg, *.png, *.gif)'
            ]
        );



        $fieldset->addField(
            'url',
            'text',
            [
                'name' => 'url',
                'label' => __('Url'),
                'title' => __('Url'),
                'required' => false,
                'class' => 'required-url'
            ]
        );
        $fieldset->addField(
            'button',
            'text',
            [
                'name' => 'button',
                'label' => __('Text Button'),
                'title' => __('Text Button'),
                'required' => false,
            ]
        );

        $fieldset->addField(
            'description',
            'editor',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'required' => false,
                'style'     => 'width:500px; height:200px;',
                'wysiwyg'   => true,
            ]
        );


        $fieldset->addField(
            'order',
            'text',
            [
                'name' => 'order',
                'label' => __('Sort Order'),
                'title' => __('Sort Order'),
                'required' => false,
            ]
        );

        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Status'),
                'title' => __('Status'),
                'values'    => array(
                    array(
                        'value'     => 0,
                        'label'     => __('Disabled'),
                    ),
                    array(
                        'value'     => 1,
                        'label'     => __('Enabled'),
                    ),
                ),
            ]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}