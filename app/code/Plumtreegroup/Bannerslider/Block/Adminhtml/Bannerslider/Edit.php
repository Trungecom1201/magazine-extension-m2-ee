<?php

namespace Plumtreegroup\Bannerslider\Block\Adminhtml\Bannerslider;

use Magento\Backend\Block\Widget\Form\Container;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Edit extends Container {

    protected $_coreRegistry = null;

    public function __construct(Context $context, Registry $registry, array $data = [])
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Plumtreegroup\Bannerslider';
        $this->_controller = 'adminhtml_bannerslider';
        parent::_construct();

        if ($this->_isAllowedAction('Plumtreegroup_Bannerslider::save')) {
            $this->buttonList->update('save', 'label', __('Save Item'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );
        } else {
            $this->buttonList->remove('save');
        }

        if ($this->_isAllowedAction('Plumtreegroup_Bannerslider::bannerslider_delete')) {
            $this->buttonList->update('delete', 'label', __('Delete Item'));
        } else {
            $this->buttonList->remove('delete');
        }
    }

    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('bannerslider_data')->getId()) {
            return __("Edit Item '%1'", $this->escapeHtml($this->_coreRegistry->registry('bannerslider_data')->getFileName()));
        } else {
            return __('New Item');
        }
    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('bannerslider/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }
}