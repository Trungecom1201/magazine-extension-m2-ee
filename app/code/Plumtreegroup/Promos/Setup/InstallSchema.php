<?php

namespace Plumtreegroup\Promos\Setup;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $promosTable = $setup->getTable('salesrule');
        if ($setup->getConnection()->isTableExists($promosTable) == true) {
            $column = [
                'type' => Table::TYPE_TEXT,
                'length' =>  255,
                'nullable' => true,
                'comment' => 'Promotion Type',
            ];
            $installer->getConnection()->addColumn('salesrule', 'promotion_type', $column);

            $column = [
                'type' => Table::TYPE_TEXT,
                'length' =>  255,
                'nullable' => true,
                'comment' => 'Auto Addtocart',
            ];
            $installer->getConnection()->addColumn('salesrule', 'auto_addtocart', $column);

            $column = [
                'type' => Table::TYPE_TEXT,
                'length' =>  255,
                'nullable' => true,
                'comment' => 'Prepare',
            ];
            $installer->getConnection()->addColumn('salesrule', 'prepare', $column);

            $column = [
                'type' => Table::TYPE_TEXT,
                'length' =>  255,
                'nullable' => true,
                'comment' => 'Cleanse',
            ];
            $installer->getConnection()->addColumn('salesrule', 'cleanse', $column);

            $column = [
                'type' => Table::TYPE_TEXT,
                'length' =>  255,
                'nullable' => true,
                'comment' => 'Treat',
            ];
            $installer->getConnection()->addColumn('salesrule', 'treat', $column);

            $column = [
                'type' => Table::TYPE_TEXT,
                'length' =>  255,
                'nullable' => true,
                'comment' => 'Associated',
            ];
            $installer->getConnection()->addColumn('salesrule', 'associated', $column);

            $column = [
                'type' => Table::TYPE_TEXT,
                'length' =>  255,
                'nullable' => true,
                'comment' => 'Associated',
            ];
            $installer->getConnection()->addColumn('salesrule', 'associated', $column);


        }
        $installer->endSetup();
    }
}