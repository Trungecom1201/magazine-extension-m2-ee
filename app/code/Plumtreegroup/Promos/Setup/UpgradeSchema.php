<?php

namespace Plumtreegroup\Promos\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context)
    {
        $installer = $setup;
        $setup->startSetup();
        if (version_compare($context->getVersion(), '0.1.2') < 0) {
            $tableName = $setup->getTable('salesrule');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' =>  255,
                    'nullable' => true,
                    'comment' => 'Apply In Cart',
                ];
                $installer->getConnection()->addColumn('salesrule', 'apply_cart', $column);
            }
        }

        if (version_compare($context->getVersion(), '0.1.1') < 0) {
            $tableName = $setup->getTable('salesrule');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' =>  255,
                    'nullable' => true,
                    'comment' => 'Number Item',
                ];
                $installer->getConnection()->addColumn('salesrule', 'number_item', $column);
            }
        }
        if(version_compare($context->getVersion(), '0.1.3') < 0){
            $tableName = $setup->getTable('quote_item');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $column = [
                    'type' => Table::TYPE_INTEGER,
                    'length' =>  11,
                    'default' => '0',
                    'comment' => 'Is Free Sample',
                ];
                $installer->getConnection()->addColumn($tableName, 'free_sample', $column);
            }
            $tableName = $setup->getTable('sales_order_item');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $column = [
                    'type' => Table::TYPE_INTEGER,
                    'length' =>  11,
                    'default' => '0',
                    'comment' => 'Is Free Sample',
                ];
                $installer->getConnection()->addColumn($tableName, 'free_sample', $column);
            }
        }

        $setup->endSetup();
    }
}