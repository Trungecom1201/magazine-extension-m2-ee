<?php

namespace Plumtreegroup\Promos\Helper;

use Zend\Form\Annotation\Object;

class Data extends \Magento\Framework\App\Helper\AbstractHelper{

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productloader;

    protected $_ruleFactory;
    protected $_saleRuleModel;
    protected $productFactory;
    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Model\ProductFactory $productloader
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepo
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\SalesRule\Model\RuleFactory $ruleFactory,
        \Magento\Catalog\Model\ProductFactory $productloader,
        \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory $saleRuleModel,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ){
        parent::__construct($context);
        $this->_productRepository = $productRepository;
        $this->_ruleFactory = $ruleFactory;
        $this->_productloader = $productloader;
        $this->_saleRuleModel = $saleRuleModel;
        $this->productFactory = $productFactory;
    }

    public function getProductBySku($SkuProduct){
        $product = $this->productFactory->create();
        return $product->load($product->getIdBySku($SkuProduct));
    }

    /**
     * @return \Magento\SalesRule\Model\ResourceModel\Rule\Collection
     */
    public function getCollectionSaleRule($ruleids = null){
        $Collection = $this->_saleRuleModel->create();
        $Collection->addFieldToFilter('apply_cart','1');
        if($ruleids){
            $Collection->addFieldToFilter('rule_id',['in'=> explode(',',$ruleids)]);
        }else{
            $Collection->addFieldToFilter('rule_id',0);
        }
        return $Collection->getFirstItem();
    }

    public function loadCollectionSaleRule()
    {
        $param = $this->getRequest()->getParam('id');
         return $this->_ruleFactory->create()->load($param);

    }

    public function formatAssociated($skus)
    {
        $arraySku = [];
        foreach (explode(',',$skus) as $sku)
        {
            $sku = explode('---',$sku);
            if(count($sku) > 1){
                $arraySku[] = $sku[0];
            }
        }
        return $arraySku;
    }
}