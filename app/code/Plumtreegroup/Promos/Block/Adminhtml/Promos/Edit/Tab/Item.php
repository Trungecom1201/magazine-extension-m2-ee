<?php

namespace Plumtreegroup\Promos\Block\Adminhtml\Promos\Edit\Tab;

class Item extends \Magento\Backend\Block\Template
{
    protected $_template = 'Plumtreegroup_Promos::promos/item.phtml';

    protected $_coreRegistry;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry
    )
    {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    public function getCurrentRule(){
        $rule = $this->_coreRegistry->registry(\Magento\SalesRule\Model\RegistryConstants::CURRENT_SALES_RULE);
        return $rule;
    }
}