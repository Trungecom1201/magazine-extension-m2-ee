<?php
namespace Plumtreegroup\Promos\Block\Freesamples;
use Plumtreegroup\Promos\Helper\Data;

class GiftRegimen extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_saleRuleModel;

    protected $_helperFunction;

    protected $imageHelper;

    protected $cart;

    protected $helperCart;

    protected $sku = [];

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumtreegroup\Promos\Helper\Data  $helperFunction,
        \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory $saleRuleModel,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Checkout\Helper\Cart $helperCart,
        array $data = [])
    {
        $this->_helperFunction = $helperFunction;
        $this->_saleRuleModel = $saleRuleModel;
        $this->imageHelper = $imageHelper;
        $this->cart = $cart;
        $this->helperCart = $helperCart;
        parent::__construct($context);
    }

    /**
     * @return \Magento\SalesRule\Model\ResourceModel\Rule\Collection
     */
    public function getCollectionSaleRule(){
        return $this->_helperFunction->getCollectionSaleRule($this->cart->getQuote()->getAppliedRuleIds());
    }


    /**
     * @param $sku
     * @return $this
     */
    public function getCollectionProductBySku($sku)
    {
        return $this->_helperFunction->getProductBySku($sku);
    }

    public function getStoreUrl()
    {
        $storeId = $this->_storeManager->getDefaultStoreView()->getStoreId();
        return $this->_storeManager->getStore($storeId)->getUrl();
    }

    public function getImage($product)
    {
        return  $this->imageHelper->init($product, 'product_small_image')->getUrl();
    }

    public function getSkuInCart($sku,$type,$tab)
    {
        $isChecked = false;
        $itemsVisible = $this->cart->getQuote()->getAllVisibleItems();
        foreach ($itemsVisible as $item)
        {
            $info_buyRequest = unserialize($item->getOptionByCode('info_buyRequest')->getValue());
            if(isset($info_buyRequest['type']) && $sku == $item->getSku()){
                if($info_buyRequest['type'] == \Plumtreegroup\Promos\Model\Option\AutoOptions::XML_GIFT_REGIMEN){
                    if($info_buyRequest['tab'] == $tab){
                        $isChecked = true;
                    }
                }else{
                    if($type ==  $info_buyRequest['type']){
                        $isChecked = true;
                    }
                }
            }
        }
        return $isChecked;
    }

    public function isChecked($sku,$type = 1,$tab = null)
    {
        if($this->getSkuInCart($sku,$type,$tab)){
            return 'checked="checked"';
        }
        return '';
    }

    public function getCountCartItem()
    {
        return $this->helperCart->getItemsCount();
    }

    public function formatSku($skus)
    {
        return $this->_helperFunction->formatAssociated($skus);
    }

}