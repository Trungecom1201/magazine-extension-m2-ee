<?php

namespace Plumtreegroup\Promos\Controller\Promos;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Framework\Exception\NoSuchEntityException;

class Add extends \Magento\Checkout\Controller\Cart
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    protected $_collection;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param ProductRepositoryInterface $productRepository
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        CustomerCart $cart,
        ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
    ) {
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart
        );
        $this->productRepository = $productRepository;
        $this->_collection = $collection;
    }

    /**
     * Initialize product instance from request data
     *
     * @return \Magento\Catalog\Model\Product|array
     */
    protected function _initProductIds($params)
    {
        if ($params['skus']) {
            try {
                $collection = $this->_collection->load();
                $collection->addAttributeToFilter('sku',['in' => $params['skus']]);
                if(!empty($collection->getAllIds())){
                    return $collection->getAllIds();
                }
                return [];
            } catch (NoSuchEntityException $e) {
                return [];
            }
        }
        return [];
    }


    
    public function execute()
    {
        try {
            $params = $this->formatParams($this->getRequest()->getParams());
            if(!empty($this->_initProductIds($params))){
                $this->cart->customAddProductsByIds($this->_initProductIds($params),$params);
                $this->cart->save();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {

        }
    }

    protected function formatParams($params)
    {
        if($params['type'] == \Plumtreegroup\Promos\Model\Option\AutoOptions::XML_GIFT_REGIMEN){
            $skuNew = [];
            $skuOld = [];
            foreach ($params['skus'] as $sku)
            {
                $sku = explode('---',$sku);
                $skuNew[$sku[1]][] = $sku[0];
                $skuOld[] = $sku[0];
            }
            $params['skus'] = $skuOld;
            $params['format_sku'] = $skuNew;
            return $params;
        }
        return $params;
    }
}