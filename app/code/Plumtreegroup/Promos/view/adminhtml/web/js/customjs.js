define([
    'jquery'
], function ($) {
    $(document).ready(function () {


        var _form_html_row = '<tr class="option-row plumpromo-assoc-sku" id="hour-row-{{id}}">' +
            '<td><input name="associal[value][option_{{id}}][sku]"' +
            '  id="option-{{id}}" class="input-text option-acc class-option-{{id}}" type="text"></td><td>' +
            '<input name="associal[value][option_{{id}}][promprice]" ' +
            ' class="input-text option-acc" id="promprice-option-{{id}}" value="0" type="text"></td><td>' +
            '  <input id="delete-row-{{id}}" type="hidden" class="delete-flag" ' +
            'name="associal[delete][option_{{id}}][del]" value=""/>' +
            '<button onclick="" title="Delete" type="button" ' +
            'class="scalable delete delete-option"><span><span><span>Delete</span></span></span></button>  </td></tr>';

        var countRows = $('#attribute-options-table').find('.option-row').length;

        $('#add_new_option_button').on('click', function () {
            countRows++;
            $('#attribute-options-table').append(_form_html_row.replace(/\{\{id\}\}/ig, countRows));
        });

        $('.delete-option').on('click',function () {
            changeVal();
        });

        function changeVal() {
            var a = "";
            var inputElement = $("input[name*='associated']");
            $('#attribute-options-table').find('.option-acc').each(function(){
                var attrId = $(this).attr('id');
                if($('#promprice-'+attrId).length && typeof $('#promprice-'+attrId) !== "undefined"){
                    var discount = $('#promprice-'+attrId).val();
                    var value = this.value;
                    if (value) {
                        a += value+'---'+discount+',';
                    }
                }
            });
            inputElement.val(a);
            inputElement.trigger('change');
        }

        $('#attribute-options-table').delegate('.option-acc','change', function () {
            changeVal();
        });
        $('#attribute-options-table').delegate('.delete-option','click', function () {
            $(this).parent().parent().remove();
            changeVal();
        });
    });
});