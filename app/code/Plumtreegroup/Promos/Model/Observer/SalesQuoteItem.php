<?php

namespace Plumtreegroup\Promos\Model\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesQuoteItem  implements ObserverInterface
{
    protected $_objectManager;

    protected $_helper;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Plumtreegroup\Promos\Helper\Data $helper
    ) {
        $this->_objectManager = $objectManager;
        $this->_helper = $helper;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getProduct();
        $item = $observer->getQuoteItem();
        $rule = $this->_helper->getCollectionSaleRule($item->getAppliedRuleIds());
        if($product->getFreeSample() && $rule){
            $item->setFreeSample(1);
            if($rule == 'free'){
                $item->setFreeSample(1);
                $item->setCustomPrice(0);
                $item->setOriginalCustomPrice(0);
                $item->getProduct()->setIsSuperMode(true);
            }elseif ($rule->getPromotionType() == 'discount'){
                $regi = \Plumtreegroup\Promos\Model\Option\AutoOptions::XML_GIFT_REGIMEN;
                if($rule->getAutoAddtocart() != $regi){
                    $discountSkus = $this->formatSkus($rule->getAssociated());
                    if($discountSkus && isset($discountSkus[$product->getSku()])){
                        $discount = $discountSkus[$product->getSku()];
                        $priceDiscount = $product->getPrice() - ($product->getPrice() * ($discount/100));
                        $item->setCustomPrice($priceDiscount);
                        $item->setOriginalCustomPrice($priceDiscount);
                        $item->getProduct()->setIsSuperMode(true);
                    }
                }
            }
        }
    }

    protected function formatSkus($skus)
    {
        $arrayDiscountSku = [];
        foreach (explode(',',$skus) as $sku)
        {
            $sku = explode('---',$sku);
            if(count($sku) > 1){
                $arrayDiscountSku[$sku[0]] = $sku[1];
            }
        }
        return $arrayDiscountSku;
    }
}
