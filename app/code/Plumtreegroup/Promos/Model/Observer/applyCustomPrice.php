<?php

namespace Plumtreegroup\Promos\Model\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Cart as CustomerCart;

class applyCustomPrice  implements ObserverInterface
{
    protected $_objectManager;

    protected $_helperFunc;
    protected $_helperFunction;
    protected $cart;
    protected $_collection;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Plumtreegroup\Promos\Helper\Data $helper,
        \Plumtreegroup\Promos\Helper\Data  $helperFunction,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection,
        CustomerCart $cart
    ) {
        $this->_objectManager = $objectManager;
        $this->_helperFunction = $helper;
        $this->_collection = $collection;
        $this->cart = $cart;

    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        /**
         * Custom add simple
         */
        $rule = $this->_helperFunction->getCollectionSaleRule($this->cart->getQuote()->getAppliedRuleIds());
        if($rule->getAutoAddtocart() == \Plumtreegroup\Promos\Model\Option\AutoOptions::XML_GIFT_FREE && $rule){
            $existFreeSample = false;

            foreach ($this->cart->getQuote()->getAllVisibleItems() as $item)
            {
                if($item->getFreeSample()){
                    $existFreeSample = true;
                    break;
                }
            }
            if(!$existFreeSample){
                $productSkus = $this->_helperFunction->formatAssociated($rule->getAssociated());
                $products = $this->_collection->load();
                $products->addAttributeToFilter('sku',['in' => $productSkus]);
                if(!empty($products->getAllIds())){
                    $this->cart->customAddProductsByIds($products->getAllIds(),['type' => \Plumtreegroup\Promos\Model\Option\AutoOptions::XML_GIFT_FREE]);
                }
            }

        }
    }
}
