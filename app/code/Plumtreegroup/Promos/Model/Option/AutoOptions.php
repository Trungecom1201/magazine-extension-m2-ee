<?php

namespace Plumtreegroup\Promos\Model\Option;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

use Magento\Backend\Model\Auth\Session;

class AutoOptions extends AbstractSource
{
    const XML_GIFT_REGIMEN = 3;
    const XML_GIFT_DELUXE = 2;
    const XML_GIFT_NORMAL = 1;
    const XML_GIFT_FREE = 0;

    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory
     */
    protected $_typeCollectionFactory;
    /**
     * @var Session
     */
    protected $_authSession;

    /**
     * OptionsMagazine constructor.

     * @param Session $authSession
     */
    public function __construct(
        Session $authSession
    )
    {
        $this->_authSession = $authSession;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $options = array();

        $options[] = [
            'label' => 'Gift-Regimen',
            'value' => self::XML_GIFT_REGIMEN
        ];
        $options[] = [
            'label' => 'Gift-Deluxe',
            'value' => self::XML_GIFT_DELUXE
        ];
        $options[] = [
            'label' => 'Gift-Normal',
            'value' => self::XML_GIFT_NORMAL
        ];
        $options[] = [
            'label' => 'Yes',
            'value' => self::XML_GIFT_FREE
        ];

        return $options;
    }

}