<?php

namespace Plumtreegroup\Promos\Model\Option;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

use Magento\Backend\Model\Auth\Session;

class TypeOptions extends AbstractSource
{
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory
     */
    protected $_typeCollectionFactory;
    /**
     * @var Session
     */
    protected $_authSession;

    /**
     * OptionsMagazine constructor.

     * @param Session $authSession
     */
    public function __construct(
        Session $authSession
    )
    {
        $this->_authSession = $authSession;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $options = array();

        $options[] = [
            'label' => 'Free',
            'value' => 'free'
        ];
        $options[] = [
            'label' => 'Discount',
            'value' => 'discount'
        ];


        return $options;
    }

}