<?php

namespace Plumtreegroup\Promos\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Promos extends AbstractDb {

    protected function _construct()
    {
        $this->_init('plumtreegroup_promos', 'id_promos');
    }
}