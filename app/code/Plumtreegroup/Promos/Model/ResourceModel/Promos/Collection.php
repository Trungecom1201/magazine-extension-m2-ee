<?php

namespace Plumtreegroup\Promos\Model\ResourceModel\Promos;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected $_idFieldName = 'id_promos';
    public function _construct()
    {
        $this->_init('Plumtreegroup\Promos\Model\Promos', 'Plumtreegroup\Promos\Model\ResourceModel\Promos');
    }

}