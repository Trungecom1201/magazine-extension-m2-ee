<?php

namespace Plumtreegroup\Promos\Model;

use Magento\Framework\Model\AbstractModel;

class Promos extends AbstractModel
{

    protected function _construct()
    {
        $this->_init('Plumtreegroup\Promos\Model\ResourceModel\Promos');
    }


}