<?php
namespace Plumtreegroup\Promos\Model\Rewrite;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\DataObject;

class Cart extends \Magento\Checkout\Model\Cart
{

    /**
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Checkout\Model\ResourceModel\Cart $resourceCart
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockStateInterface $stockState
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param ProductRepositoryInterface $productRepository
     * @param array $data
     * @codeCoverageIgnore
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\ResourceModel\Cart $resourceCart,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        ProductRepositoryInterface $productRepository,
        array $data = []
    ) {
        parent::__construct($eventManager,$scopeConfig,$storeManager,$resourceCart,$checkoutSession,$customerSession,$messageManager,$stockRegistry,$stockState,$quoteRepository,$productRepository,$data);
    }
    /**
     * Adding products to cart by ids
     * @param $productIds
     * @param $params
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function customAddProductsByIds($productIds,$params)
    {
        $allAvailable = true;
        $allAdded = true;

        if (!empty($productIds)) {
            foreach ($this->getQuote()->getAllVisibleItems() as $item){
                if($item->getFreeSample()){
                    $this->removeItem($item->getId());
                }
            }
            /**
             * Litmit item
             */
//            $count = 0 ;
//            $reg = \Plumtreegroup\Promos\Model\Option\AutoOptions::XML_GIFT_REGIMEN;
//            $limitItem =  $this->helperFunction->getCollectionSaleRule()->getNumberItem();
//            $limit = $this->helperFunction->getCollectionSaleRule()->getAutoAddtocart() == $reg ? $limitItem * 3 : $limitItem;
            foreach ($productIds as $productId) {
                $productId = (int)$productId;
                if (!$productId) {
                    continue;
                }
//                $count++;
//                if($count > $limit){
//                    break;
//                }
                $product = $this->_getProduct($productId);
                if ($product->getId() && $product->isVisibleInCatalog()) {
                    try {
                        $type['type'] = $params['type'];
                        $type['product'] = $productId;
                        if(isset($params['format_sku'])){
                            foreach ($params['format_sku'] as $key => $value)
                            {
                                if(in_array($product->getSku(),$value)){
                                    $type['tab'] = $key;
                                }
                            }
                        }
                        $request = $this->_getProductRequest($type);
                        $product->setFreeSample(true);
                        $this->getQuote()->addProduct($product,$request);
                    } catch (\Exception $e) {
                        $allAdded = false;
                    }
                } else {
                    $allAvailable = false;
                }
            }

            if (!$allAvailable) {
                $this->messageManager->addError(__("We don't have some of the products you want."));
            }
            if (!$allAdded) {
                $this->messageManager->addError(__("We don't have as many of some products as you want."));
            }
        }
        return $this;
    }
}
