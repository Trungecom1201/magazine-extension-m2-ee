<?php

namespace Plumtreegroup\Magazine\Ui\Component\Videomagazine;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Reporting;
use Magento\Framework\Api\Search\SearchResultInterface;


class DataProviderGrid extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
{
    protected $authSession;
    const USER_ROLE_ADMINISTRATORS = 'Administrators';
    const CUSTOMER_EAV_ATTRIBUTE = 'customer';

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $_eavAttribute;

    /**
     * DataProviderGrid constructor.
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param string $name
     * @param string $primaryFieldName
     * @param \Magento\Framework\Api\Search\ReportingInterface $requestFieldName
     * @param Reporting $reporting
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param RequestInterface $request
     * @param \Magento\Backend\Model\Session $authSession
     * @param FilterBuilder $filterBuilder
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
        $name,
        $primaryFieldName,
        $requestFieldName,
        Reporting $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        \Magento\Backend\Model\Session $authSession,
        FilterBuilder $filterBuilder,
        array $meta = [],
        array $data = []
    ) {
        $this->authSession = $authSession;
        $this->_eavAttribute = $eavAttribute;
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $reporting,
            $searchCriteriaBuilder,
            $request,
            $filterBuilder,
            $meta,
            $data
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = parent::getData();
        return $data;
    }

    /**
     * @param SearchResultInterface $searchResult
     * @return array
     */
    protected function searchResultToOutput(SearchResultInterface $searchResult)
    {
        $searchResult->getSelect()->join(array('vtm' => 'video_type_magazine'),
            "main_table.video_type_id = vtm.video_type_id",
            array('video_type_name'));
        $arrItems = [];

        $arrItems['items'] = [];
        foreach ($searchResult->getItems() as $item) {
            $itemData = [];
            foreach ($item->getCustomAttributes() as $attribute) {
                $itemData[$attribute->getAttributeCode()] = $attribute->getValue();
            }
            $arrItems['items'][] = $itemData;
        }
        $arrItems['totalRecords'] = $searchResult->getTotalCount();

        return $arrItems;
    }

}
