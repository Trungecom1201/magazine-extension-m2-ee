<?php
namespace Plumtreegroup\Magazine\Ui\Component\Numbermagazine\DataProviderGrid;

use Magento\Framework\Api\AttributeValueFactory;

class Document extends \Magento\Framework\View\Element\UiComponent\DataProvider\Document
{

    /**
     * Document constructor.
     * @param AttributeValueFactory $attributeValueFactory
     */
    public function __construct(
        AttributeValueFactory $attributeValueFactory
    ) {
        parent::__construct($attributeValueFactory);
    }



}
