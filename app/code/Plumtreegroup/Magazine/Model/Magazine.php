<?php
namespace Plumtreegroup\Magazine\Model;


class Magazine extends \Magento\Framework\Model\AbstractModel
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**
     * @var ResourceModel\Numbermagazine\CollectionFactory
     */
    protected $magazineFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Magazine constructor.
     * @param \Magento\Framework\Registry $coreRegistry
     * @param ResourceModel\Numbermagazine\CollectionFactory $magazineFactory
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory $magazineFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->magazineFactory = $magazineFactory;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);

    }

    /**
     * get current product
     * @return mixed
     */
    public function getCurrentProduct(){
        return $this->_coreRegistry->registry('current_product');
    }


    /**
     * get magazine asssigned to current product
     * @return $this
     */
    public function getMagazineInCurrentProduct(){

        $currentProduct = $this->getCurrentProduct();
        $magazineCollection = $this->magazineFactory->create()->addFieldToSelect('*')
            ->addFieldToFilter('products', array('finset' => $currentProduct->getId()));

        return $magazineCollection;
    }

    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
}