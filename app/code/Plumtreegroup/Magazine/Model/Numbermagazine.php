<?php

namespace Plumtreegroup\Magazine\Model;

use Magento\Framework\Model\AbstractModel;

class Numbermagazine extends AbstractModel
{

    protected function _construct()
    {
        $this->_init('Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine');
    }


}