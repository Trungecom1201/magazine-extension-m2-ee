<?php

namespace Plumtreegroup\Magazine\Model\ResourceModel\Typevideo;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected $_idFieldName = 'video_type_id';
    public function _construct()
    {
        $this->_init('Plumtreegroup\Magazine\Model\Typevideo', 'Plumtreegroup\Magazine\Model\ResourceModel\Typevideo');
    }

}