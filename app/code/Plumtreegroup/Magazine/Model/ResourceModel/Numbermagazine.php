<?php

namespace Plumtreegroup\Magazine\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Numbermagazine extends AbstractDb {

    protected function _construct()
    {
        $this->_init('magazine_number', 'id_magazine_number');
    }
}