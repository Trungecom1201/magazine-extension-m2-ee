<?php

namespace Plumtreegroup\Magazine\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Typevideo extends AbstractDb {

    protected function _construct()
    {
        $this->_init('video_type_magazine', 'video_type_id');
    }
}