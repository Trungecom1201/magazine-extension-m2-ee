<?php

namespace Plumtreegroup\Magazine\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Typemagazine extends AbstractDb {

    protected function _construct()
    {
        $this->_init('cus_type_magazine', 'id_type');
    }
}