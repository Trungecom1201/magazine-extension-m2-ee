<?php

namespace Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected $_idFieldName = 'id_type';
    public function _construct()
    {
        $this->_init('Plumtreegroup\Magazine\Model\Typemagazine', 'Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine');
    }

}