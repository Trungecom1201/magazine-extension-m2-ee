<?php

namespace Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected $_idFieldName = 'video_id';
    public function _construct()
    {
        $this->_init('Plumtreegroup\Magazine\Model\Videomagazine', 'Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine');
    }

    /**
     * @return array all year Magazine
     */
    public function getAllYearMagazine(){
        $select = $this->getSelect();
        $select->reset(\Magento\Framework\DB\Select::COLUMNS);
        $select->columns('YEAR(post_date)');
        $select->where('YEAR(post_date)');
        $select->group('YEAR(post_date)');
        $select->order('YEAR(post_date) DESC');
        $results = $this->getResource()->getConnection()->fetchAll($select);
        return $results;
    }

    /**
     * @param $idType
     * @param $idProduct
     * @param $mYear
     * @param $page
     * @return array Collection magazine
     */
    public function getCollectionFilterVideoMagazine($idType, $idProduct, $mYear, $page, $name)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('Plumtreegroup\Magazine\Helper\Data');
        if (!empty($idType)) {
            $this->addFieldToFilter('video_type_id',array('in'=>$idType));
        }
        if (!empty($idProduct)) {
            $sql = null;
            $count = 0;
            foreach (explode(',', $idProduct) as $productId) {
                if ( $count == 0){
                    $sql .= ' FIND_IN_SET('.(int)$productId.',products)';
                } else {
                    $sql .= ' OR FIND_IN_SET('.(int)$productId.',products)';
                }
                $count ++;

            }
            $this->getSelect()->where($sql);
        }
        if (!empty($mYear)) {
            $this->getSelect()->where('YEAR(`post_date`) IN ('.$mYear.')');
        }
        if (!empty($name)) {
            $this->addFieldToFilter('video_name',array('in'=>$name));
        }
        if($page >= 1){
            $numberItem = $helper->getNumberVideoCanShow() * $page;
            $this->getSelect()->limit($numberItem);
        }else{
            $this->getSelect()->limit($helper->getNumberVideoCanShow());
        }
        $this->getSelect()->order('post_date DESC');
        $this->addFieldToFilter('video_status', '1');
        return $this->getData();
    }
}