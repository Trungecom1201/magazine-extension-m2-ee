<?php
namespace Plumtreegroup\Magazine\Model\Option;
class Website implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Plumtreegroup\Magazine\Model\Magazine
     */
    protected $_websiteCollectionFactory;

    /**
     * StatusItem constructor.
     * @param \Plumtreegroup\Magazine\Model\Magazine $status
     */
    public function __construct(
        \Magento\Store\Model\ResourceModel\Website\CollectionFactory $websiteCollectionFactory
        )
    {

        $this->_websiteCollectionFactory = $websiteCollectionFactory;
    }
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        $collection = $this->_websiteCollectionFactory->create();
        $options[0] = ['label'=> 'All Store', 'value'=>'0'];
        foreach ($collection as $item) {
            $options[] = [
                'label' => $item->getName(),
                'value' => $item->getWebsiteId()
            ];
        }
        return $options;
    }
}