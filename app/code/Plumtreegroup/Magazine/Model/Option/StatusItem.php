<?php
namespace Plumtreegroup\Magazine\Model\Option;
class StatusItem implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Plumtreegroup\Magazine\Model\Magazine
     */
    protected $status;

    /**
     * StatusItem constructor.
     * @param \Plumtreegroup\Magazine\Model\Magazine $status
     */
    public function __construct(\Plumtreegroup\Magazine\Model\Magazine $status)
    {
        $this->status = $status;
    }
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->status->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}