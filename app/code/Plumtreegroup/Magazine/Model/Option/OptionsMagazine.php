<?php

namespace Plumtreegroup\Magazine\Model\Option;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

use Magento\Backend\Model\Auth\Session;

class OptionsMagazine extends AbstractSource
{
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory
     */
    protected $_typeCollectionFactory;
    /**
     * @var Session
     */
    protected $_authSession;

    /**
     * OptionsMagazine constructor.
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory $typeCollectionFactory
     * @param Session $authSession
     */
    public function __construct(
        \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory $typeCollectionFactory,
        Session $authSession)
    {
        $this->_typeCollectionFactory = $typeCollectionFactory;
        $this->_authSession = $authSession;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $options = array();

        $collection = $this->_typeCollectionFactory->create();

        if (count($collection) > 0) {
            foreach ($collection as $item) {
                $options[] = [
                    'label' => $item->getTypeName(),
                    'value' => $item->getIdType()
                ];
            }
        }
        return $options;
    }

}