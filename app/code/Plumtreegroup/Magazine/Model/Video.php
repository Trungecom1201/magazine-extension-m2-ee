<?php
namespace Plumtreegroup\Magazine\Model;


class Video extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var ResourceModel\Videomagazine\CollectionFactory
     */
    protected $_videoMagazineFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Magazine constructor.
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory $magazineFactory
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory $videoMagazineFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_videoMagazineFactory = $videoMagazineFactory;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);

    }

    /**
     * get current product
     * @return mixed
     */
    public function getCurrentProduct(){
        return $this->_coreRegistry->registry('current_product');
    }


    /**
     * get magazine asssigned to current product
     * @return $this
     */
    public function getVideoMagazineInCurrentProduct(){

        $currentProduct = $this->getCurrentProduct();
        $videoMagazineCollection = $this->_videoMagazineFactory->create()->addFieldToSelect('*')
            ->addFieldToFilter('products', array('finset' => $currentProduct->getId()));
        return $videoMagazineCollection;
    }

}