<?php

namespace Plumtreegroup\Magazine\Block;
class Breacrum extends \Magento\Framework\View\Element\Template{
    /**
     * @var \Plumtreegroup\Magazine\Helper\Data
     */
    protected $_helperMagazine;

    /**
     * Breacrum constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Plumtreegroup\Magazine\Helper\Data $helperMagazine
     */
    public function __construct(

        \Magento\Framework\View\Element\Template\Context $context,
        \Plumtreegroup\Magazine\Helper\Data $helperMagazine
        )
    {
        $this->_helperMagazine = $helperMagazine;
        parent::__construct($context);
    }

    public function getRouterMagazine()
    {
        return $this->_helperMagazine->getFrontNameMagazine();
    }

    public function getRouterVideo()
    {
        return $this->_helperMagazine->getFrontNameVideo();
    }
}