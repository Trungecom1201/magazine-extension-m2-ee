<?php

namespace Plumtreegroup\Magazine\Block\Widget;

class CustomWidget extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{

    /**
     * @var \Plumtreegroup\Magazine\Helper\Data
     */
    protected $_helperMagazine;
    /**
     * @var
     */
    protected $_resources;
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory
     */
    protected $magazineCollection;
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory
     */
    protected $_typeCollection;
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory
     */
    protected $videoMagazineCollection;
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory
     */
    protected $_videoTypeCollection;
    /**
     * @var \Magento\Directory\Block\Data
     */
    protected $_directoryBlock ;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * SliderMagazine constructor.
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Plumtreegroup\Magazine\Helper\Data $helperMagazine
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory $magazineCollection
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory $typeCollection
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory $videoMagazineCollection
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory $videoTypeCollection
     * @param \Magento\Directory\Block\Data $directoryBlock
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumtreegroup\Magazine\Helper\Data $helperMagazine,
        \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory $magazineCollection,
        \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory $typeCollection,
        \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory $videoMagazineCollection,
        \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory $videoTypeCollection,
        \Magento\Directory\Block\Data $directoryBlock,array $data = [])
    {
        $this->registry = $registry;
        $this->_helperMagazine = $helperMagazine;
        $this->magazineCollection = $magazineCollection;
        $this->_typeCollection = $typeCollection;
        $this->videoMagazineCollection = $videoMagazineCollection;
        $this->_videoTypeCollection = $videoTypeCollection;
        $this->_directoryBlock = $directoryBlock;
        parent::__construct($context);
    }
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('widget/custom_widget.phtml');
    }
    /**
     * get current product
     * @return mixed
     */
    public function getCurrentProduct()
    {
        return $this->registry->registry('current_product');
    }

    /**
     * get magazine where current product
     * @return $this
     */
    public function getMagazine()
    {

        $magazineCollection = $this->magazineCollection->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('magazine_status', '1')
            ->addFieldToFilter('magazine_widget', '1')
            ->setOrder('position', 'DESC');
        return $magazineCollection;
    }
    public function getVideoMagazine()
    {
        $videoMagazineCollection = $this->videoMagazineCollection->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('video_status', '1')
            ->addFieldToFilter('video_widget', '1')
            ->setOrder('position', 'DESC');
        return $videoMagazineCollection;
    }

    /**
     * @param $nameImage
     * @return string url image
     */
    public function getMediapath($nameImage){
        return $this->_helperMagazine->getImageUrl($nameImage);
    }

    /**
     * @param $year
     * @return string year
     */
    public function getYear($year){
        $yearFormat = $this->_helperMagazine->getFormatDateMagazineToYear($year);
        return $yearFormat;
    }

    /**
     * @param $month
     * @return string month
     */
    public function getMonth($month){
        $monthFormat = $this->_helperMagazine->getFormatDateMagazineToMonth($month);
        return $monthFormat;
    }

}