<?php

namespace Plumtreegroup\Magazine\Block;
class FilterMagazine extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Directory\Block\Data
     */
    protected $_directoryBlock ;

    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory
     */
    protected $_magazineModel ;

    /**
     * @var \Plumtreegroup\Magazine\Helper\Data
     */
    protected $_helperMagazine;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * FilterMagazine constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Plumtreegroup\Magazine\Helper\Data $helper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\Collection $magazineModel
     * @param \Magento\Directory\Block\Data $directoryBlock
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumtreegroup\Magazine\Helper\Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\Collection $magazineModel,
        \Magento\Directory\Block\Data $directoryBlock,array $data = [])
    {
        $this->_magazineModel = $magazineModel;
        $this->_storeManager = $storeManager;
        $this->_directoryBlock = $directoryBlock;
        $this->_helperMagazine = $helper;
        parent::__construct($context);
    }

    /**
     * @return \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\Collection
     */
    public function getAllNumberMagazine(){
        $params = $this->getParam();

        $Collection = $this->_magazineModel->getCollectionFilterMagazine($params['type'],$params['product'],$params['year'],$params['page']);
        return $Collection;
    }

    /**
     * @param $nameImage
     * @return path image URL
     */
    public function getMediapath($nameImage){
        return $this->_helperMagazine->getImageUrl($nameImage);
    }

    /**
     * @param $idproduct
     * @return product collection
     */
    public function getProductById($id){
        $productCollection = $this->_helperMagazine->getProductByEntityId($id);
        return $productCollection;
    }

    /**
     * @param created at
     * @return year
     */
    public function getYear($year){
        $yearFormat = $this->_helperMagazine->getFormatDateMagazineToYear($year);
        return $yearFormat;
    }

    /**
     * @param created at
     * @return $month
     */
    public function getMonth($month){
        $monthFormat = $this->_helperMagazine->getFormatDateMagazineToMonth($month);
        return $monthFormat;
    }

    /**
     * @return array
     */
    public function getParam(){
        $params = $this->getRequest()->getParams();
        return $params;
    }

    public function getNumberMagazineCanShow(){
        $number = $this->_helperMagazine->getNumberItemCanShow();
        if(isset($number)){
            return $number;
        }else{
            return '12';
        }
    }
    public function getParamItem(){
        $params = $this->getRequest()->getParams();
        return $params['page'];
    }
    /**
     * @return $this
     */
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}