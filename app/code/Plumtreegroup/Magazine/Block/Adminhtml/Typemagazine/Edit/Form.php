<?php
namespace Plumtreegroup\Magazine\Block\Adminhtml\Typemagazine\Edit;

use Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic {

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * Form constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('type_magazine_form');
        $this->setTitle(__('Type Press ').'<span class="sp-highlight-term">'.__('Information').'</span>)');

    }

    /**
     * Prepare form
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('type_magazine_data');

        $form = $this->_formFactory->create(
            [
                'data' => [
                'id' => 'edit_form',
                'action' => $this->getData('action'),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
                ]
            ]
        );

        $form->setHtmlIdPrefix('magazine_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General <span class="sp-highlight-term">Information</span>'), 'class' => 'fieldset-wide']
        );
        if ($model->getIdType()) {
            $fieldset->addField('id_type', 'hidden', ['name' => 'id_type']);
        }

        $fieldset->addField(
            'type_name',
            'text',
            ['name' => 'type_name', 'label' => __('Name'), 'title' => __('Name'), 'required' => true]
        );


        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}