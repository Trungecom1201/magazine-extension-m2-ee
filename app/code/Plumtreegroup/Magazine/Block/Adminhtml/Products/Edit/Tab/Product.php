<?php
namespace Plumtreegroup\Magazine\Block\Adminhtml\Products\Edit\Tab;


class Product extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory
     */
    protected $magazineFactory;

    /**
     * @var \Plumtreegroup\Magazine\Model\Magazine
     */
    protected $magazine;

    public function __construct(
        \Plumtreegroup\Magazine\Model\Magazine $magazine,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory $magazineFactory,

        array $data = []
    ) {
        $this->magazine = $magazine;
        $this->magazineFactory = $magazineFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('catalog_category_magazines');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }


    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        parent::_addColumnFilterToCollection($column);
        return $this;
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection()
    {
        $collection = $this->magazineFactory->create();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_category',
            [
                'type' => 'checkbox',
                'name' => 'in_category',
                'values' => $this->_getSelectedProducts(),
                'index' => 'id_magazine_number',
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction'
            ]
        );
        $this->addColumn(
            'id_magazine_number',
            [
                'header' => __('ID'),
                'sortable' => true,
                'index' => 'id_magazine_number',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'name_number',
            [
                'header' => __('Name'),
                'index' => 'name_number',
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name'
            ]
        );

        $this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'type' => 'number',
                'index' => 'position',
                'column_css_class'=>'no-display',
                'header_css_class'=>'no-display',
                'editable' => true
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * get magazine by current product
     * @return array
     */
    public function getSelectedProducts()
    {
        $magazines = [];
        $magazineCollection = $this->magazine->getMagazineInCurrentProduct();
        if(isset($magazineCollection)){
            foreach ($magazineCollection as $magazine) {
                $magazines[] = $magazine->getId();
            }
        }

        return $magazines;
    }


    /**
     * get magazine by current product
     * @return array
     */
    public function _getSelectedProducts()
    {
        $magazines = [];
        $magazineCollection = $this->magazine->getMagazineInCurrentProduct();
        if(isset($magazineCollection)){
            foreach ($magazineCollection as $magazine) {
                $magazines[] = $magazine->getId();
            }
        }
        return $magazines;
    }
}