<?php
namespace Plumtreegroup\Magazine\Block\Adminhtml\Products\Edit;

class AssignVideo extends \Magento\Backend\Block\Template
{
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'products/assign_video.phtml';

    /**
     * @var \Magento\Catalog\Block\Adminhtml\Category\Tab\Product
     */
    protected $blockGrid;


    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Plumtreegroup\Magazine\Model\Videomagazine
     */
    protected $videoModel;
    protected  $jsonEncoder;
    /**
     * AssignBrands constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Plumtreegroup\Magazine\Model\Video $videoModel
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Plumtreegroup\Magazine\Model\Video $videoModel,
        array $data = []
        ) {
            $this->videoModel = $videoModel;
            $this->jsonEncoder = $jsonEncoder;
            parent::__construct($context, $data);
        }

    /**
     * Retrieve instance of grid block
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                'Plumtreegroup\Magazine\Block\Adminhtml\Products\Edit\Tab\ProductVideo',
                'category.product.video.grid'
            );
        }
        return $this->blockGrid;
    }

    /**
     * Return HTML of grid block
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getBlockGrid()->toHtml();
    }


    /**
     * get brand json for input hide
     * @return string
     */
    public function getVideoJson()
    {
        $videos = [];
        $videoCollection =  $this->videoModel->getVideoMagazineInCurrentProduct();

        foreach ($videoCollection as $video) {
            $videos[$video->getVideoId()] = '';
        }
        if (!empty($videos)) {
            return $this->jsonEncoder->encode($videos);
        }
        return '{}';
    }

}