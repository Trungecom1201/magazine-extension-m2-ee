<?php
namespace Plumtreegroup\Magazine\Block\Adminhtml\Products\Edit;

class AssignMagazines extends \Magento\Backend\Block\Template
{
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'products/assign_magazines.phtml';

    /**
     * @var \Magento\Catalog\Block\Adminhtml\Category\Tab\Product
     */
    protected $blockGrid;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory
     */
    protected $magazineFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Plumtreegroup\Magazine\Model\Magazine
     */
    protected $magazine;

    /**
     * AssignMagazines constructor.
     * @param \Plumtreegroup\Magazine\Model\Magazine $magazine
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory $magazineFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Plumtreegroup\Magazine\Model\Magazine $magazine,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory $magazineFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
        ) {
            $this->magazine = $magazine;
            $this->_coreRegistry = $coreRegistry;
            $this->registry = $registry;
            $this->jsonEncoder = $jsonEncoder;
            $this->magazineFactory = $magazineFactory;
            parent::__construct($context, $data);
        }

    /**
     * Retrieve instance of grid block
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                'Plumtreegroup\Magazine\Block\Adminhtml\Products\Edit\Tab\Product',
                'category.product.grid'
            );
        }
        return $this->blockGrid;
    }

    /**
     * Return HTML of grid block
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getBlockGrid()->toHtml();
    }


    /**
     * get magazine json for input hide
     * @return string
     */
    public function getMagazinesJson()
    {
        $magazineCollection = $this->magazine->getMagazineInCurrentProduct();

        $magazines = [];
        foreach($magazineCollection as $magazine){
            $magazines[$magazine->getId()]  = '';
        }

        if (!empty($magazines)) {
            return $this->jsonEncoder->encode($magazines);
        }
        return '{}';
    }

}