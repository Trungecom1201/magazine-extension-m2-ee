<?php
namespace Plumtreegroup\Magazine\Block\Adminhtml\Products\Edit\Tab;


class ProductVideo extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory
     */
    protected $videoMagazineFactory;

    /**
     * @var \Plumtreegroup\Magazine\Model\Video
     */
    protected $video;

    /**
     * Product constructor.
     * @param \Plumtreegroup\Magazine\Model\Magazine $video
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory $videoMagazineFactory
     * @param array $data
     */
    public function __construct(
        \Plumtreegroup\Magazine\Model\Video $video,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory $videoMagazineFactory,

        array $data = []
    ) {
        $this->video = $video;
        $this->videoMagazineFactory = $videoMagazineFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('catalog_category_magazines');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }


    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        parent::_addColumnFilterToCollection($column);
        return $this;
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection()
    {
        $collection = $this->videoMagazineFactory->create();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_category',
            [
                'type' => 'checkbox',
                'name' => 'in_category',
                'values' => $this->_getSelectedProducts(),
                'index' => 'video_id',
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction'
            ]
        );
        $this->addColumn(
            'video_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'index' => 'video_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'video_name',
            [
                'header' => __('Video Name'),
                'index' => 'video_name',
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name'
            ]
        );

        $this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'type' => 'number',
                'index' => 'position',
                'column_css_class'=>'no-display',
                'header_css_class'=>'no-display',
                'editable' => true
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * get magazine by current product
     * @return array
     */
    public function getSelectedProducts()
    {
        $videos = [];
        $videoMagazineCollection = $this->video->getVideoMagazineInCurrentProduct();
        if (isset($videoMagazineCollection)){
            foreach ($videoMagazineCollection as $video) {
                $videos[] = $video->getVideoId();
            }
        }

        return $videos;
    }


    /**
     * get magazine by current product
     * @return array
     */
    public function _getSelectedProducts()
    {
        $videos = [];
        $videoMagazineCollection = $this->video->getVideoMagazineInCurrentProduct();
        if(isset($videoMagazineCollection)){
            foreach ($videoMagazineCollection as $video) {
                $videos[] = $video->getVideoId();
            }
        }

        return $videos;
    }
}