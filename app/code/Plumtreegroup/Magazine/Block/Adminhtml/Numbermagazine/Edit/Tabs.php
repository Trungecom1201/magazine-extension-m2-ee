<?php

namespace Plumtreegroup\Magazine\Block\Adminhtml\Numbermagazine\Edit;


class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('magazine_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Press Tabs'));
    }

}