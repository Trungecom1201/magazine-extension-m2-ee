<?php
namespace Plumtreegroup\Magazine\Block\Adminhtml\Numbermagazine\Edit\Tab;


class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    /**
     * @var \Plumtreegroup\Magazine\Model\Option\OptionsMagazine
     */
    protected $_magazine;
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;


    /**
     * Main constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Plumtreegroup\Magazine\Model\Option\OptionsMagazine $magazine
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Plumtreegroup\Magazine\Model\Option\OptionsMagazine $magazine,
        array $data = []
    )
    {
        $this->_magazine = $magazine;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_systemStore = $systemStore;
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     *  get store
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }

    /**
     * Prepare form
     *
     * @return \Magento\Backend\Block\Widget\Form
     */

    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('number_magazine_data');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('magazine_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => (__('Press ') . '<span class="sp-highlight-term">' . __('Information') . '</span>'),
                'class' => 'fieldset-wide'
            ]
        );

        if ($model->getIdMagazineNumber()) {
            $fieldset->addField('id_magazine_number', 'hidden', ['name' => 'id_magazine_number']);
        }

        $fieldset->addField(
            'store_ids',
            'multiselect',
            [
                'name' => 'stores[]',
                'label' => __('Website'),
                'title' => __('Website'),
                'required' => true,
                'values' => $this->_systemStore->getStoreValuesForForm(false, true),
            ]
        );

        $fieldset->addField(
            'rendor_on',
            'select',
            [
                'name' => 'rendor_on',
                'label' => __('Render On'),
                'title' => __('Render On'),
                'required' => false,
                'values' => array('Press' => 'Press', 'Video' => 'Video')
            ]
        );

        $fieldset->addField(
            'id_type',
            'select',
            [
                'name' => 'id_type',
                'label' => __('Press Type'),
                'title' => __('Press Type'),
                'class' => 'required-entry',
                'values' => $this->_magazine->getAllOptions(),
                'required' => true
            ]
        );
        $fieldset->addField(
            'name_number',
            'text',
            [
                'name' => 'name_number',
                'label' => __('Post Title'),
                'title' => __('Name'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'product_line',
            'text',
            [
                'name' => 'product_line',
                'label' => __('Product Line'),
                'title' => __('Product Line'),
                'required' => false
            ]
        );

        $fieldset->addField(
            'image',
            'image',
            [
                'name' => 'image',
                'label' => __('Post Image'),
                'title' => __('Post Image'),
                'class' => 'required-entry',
                'required' => false,
                'note' => '(*.jpg, *.png, *.gif)'
            ]
        );

        $fieldset->addField(
            'big_image',
            'image',
            [
                'name' => 'big_image',
                'label' => __('Post Thumbnail'),
                'title' => __('Post Thumbnail'),
                'class' => 'required-entry',
                'required' => false,
                'note' => '(*.jpg, *.png, *.gif)'
            ]
        );

        $fieldset->addField(
            'magazine_category',
            'select',
            [
                'name' => 'magazine_category',
                'label' => __('Category'),
                'title' => __('Category'),
                'required' => false,
                'values' => array('Magazine' => 'Magazine', 'Demo' => 'Demo')
            ]
        );
        $fieldset->addField(
            'post_date',
            'date',
            [
                'name' => 'post_date',
                'label' => __('Posted Date'),
                'title' => __('Posted Date'),
                'date_format' => $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT),
                'time_format' => $this->_localeDate->getTimeFormat(\IntlDateFormatter::SHORT),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'position',
            'text',
            [
                'name' => 'position',
                'label' => __('Position'),
                'title' => __('Position'),
                'required' => false
            ]
        );
        $fieldset->addField(
            'magazine_status',
            'select',
            [
                'name' => 'magazine_status',
                'label' => __('Post Status'),
                'title' => __('Post Status'),
                'required' => false,
                'values' => array('0' => 'Disable', '1' => 'Enable')
            ]
        );


        $fieldset->addField(
            'subtitle',
            'text',
            [
                'name' => 'subtitle',
                'label' => __('Subtitle'),
                'title' => __('Subtitle'),
                'required' => false
            ]
        );

        $fieldset->addField(
            'description',
            'editor',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'required' => false,
                'style' => 'width:500px; height:200px;',
                'wysiwyg' => true,
            ]
        );

        $fieldset->addField(
            'video_panel',
            'select',
            [
                'name' => 'video_panel',
                'label' => __('Video panel'),
                'title' => __('Video panel'),
                'required' => false,
                'values' => array(
                    '0' => 'Select video panel',
                    'option 1' => 'option 1',
                    'option 2' => 'option 2')
            ]
        );

        $fieldset->addField(
            'magazine_widget',
            'select',
            [
                'name' => 'magazine_widget',
                'label' => __('Show in Widget Slider'),
                'title' => __('Show in Widget Slider'),
                'required' => false,
                'values' => array(
                    '0' => 'Not Show In Widget',
                    '1' => 'Show In Widget'
                )

            ]
        );
        $fieldset->addField(
            'url_magazine',
            'text',
            [
                'name' => 'url_magazine',
                'label' => __('URL'),
                'title' => __('URL'),
                'required' => false,
                'class'=> 'validate-url'
            ]
        );

        $fieldset->addField(
            'hover_title',
            'text',
            [
                'name' => 'hover_title',
                'label' => __('Hover Title'),
                'title' => __('Hover Title'),
                'required' => false
            ]
        );
        $fieldset->addField(
            'hover_subtitle',
            'text',
            [
                'name' => 'hover_subtitle',
                'label' => __('Hover Subtitle'),
                'title' => __('Hover Subtitle'),
                'required' => false
            ]
        );
        $fieldset->addField(
            'change_button_url_title',
            'text',
            [
                'name' => 'change_button_url_title',
                'label' => __('Change button url title'),
                'title' => __('Change button url title'),
                'required' => false
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Press Information Tab');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Press Information Tab');
    }

    /**
     * Prepare can show tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Prepare is hidden tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }


}