<?php

namespace Plumtreegroup\Magazine\Block\Adminhtml\Numbermagazine;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize form
     * Add standard buttons
     * Add "Save and Apply" button
     * Add "Save and Continue" button
     *
     * @return void
     */
    protected function _construct()
    {

        $this->_objectId = 'id_magazine_number';
        $this->_blockGroup = 'Plumtreegroup_Magazine';
        $this->_controller = 'adminhtml_numbermagazine';

        parent::_construct();

        $this->buttonList->add(
            'save_and_continue_edit',
            [
                'class' => 'save',
                'label' => __('Save and Continue Edit'),
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ]
                    ],
                ]
            ],
            10
        );

    }

    /**
     * Getter for form header text
     *
     * @return \Magento\Framework\Phrase
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('magazine/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }




}
