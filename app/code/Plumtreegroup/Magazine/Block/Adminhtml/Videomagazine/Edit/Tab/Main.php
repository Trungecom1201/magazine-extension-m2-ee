<?php
namespace Plumtreegroup\Magazine\Block\Adminhtml\Videomagazine\Edit\Tab;


class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    /**
     * @var \Plumtreegroup\Magazine\Model\Option\OptionsMagazine
     */
    protected $_magazine;
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;


    /**
     * Main constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Plumtreegroup\Magazine\Model\Option\OptionVideo $magazine
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Plumtreegroup\Magazine\Model\Option\OptionVideo $magazine,
        array $data = []
    ) {
        $this->_magazine = $magazine;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_systemStore = $systemStore;
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     *  get store
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    public function getStore(){
        return $this->_storeManager->getStore();
    }

    /**
     * Prepare form
     *
     * @return \Magento\Backend\Block\Widget\Form
     */

    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('video_magazine_data');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('magazine_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => (__('Video Press ').'<span class="sp-highlight-term">'.__('Information').'</span>'),
                'class' => 'fieldset-wide'
            ]
        );

        if ($model->getVideoId()) {
            $fieldset->addField('video_id', 'hidden', ['name' => 'video_id']);
        }

        $fieldset->addField('store_ids', 'multiselect', array(
            'name' => 'stores[]',
            'label' => __('Store View'),
            'title' => __('Store View'),
            'required' => true,
            'values' => $this->_systemStore->getStoreValuesForForm(false, true),
        ));
        $fieldset->addField(
            'video_name',
            'text',
            [   'name' => 'video_name',
                'label' => __('Show'),
                'title' => __('Show'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'post_date',
            'date',
            [
                'name' => 'post_date',
                'label' => __('Air Date'),
                'title' => __('Air Date'),
                'date_format' => $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT),
                'time_format' => $this->_localeDate->getTimeFormat(\IntlDateFormatter::SHORT),
                'required' => true,

            ]
        );
        $fieldset->addField(
            'video_type_id',
            'select',
            [
                'name' => 'video_type_id',
                'label' => __('Category'),
                'title' => __('Category'),
                'class' => 'required-entry',
                'values' => $this->_magazine->getAllOptions(),
                'required' => true
            ]
        );

        $fieldset->addField(
            'image',
            'image',
            [
                'name' => 'image',
                'label' => __('Thumbnail Image'),
                'title' => __('Thumbnail Image'),
                'class' => 'required-entry',
                'required' => true,
                'note'      => '(*.jpg, *.png, *.gif)'
            ]
        );
        $fieldset->addField(
            'video_url',
            'editor',
            [
                'name' => 'video_url',
                'label' => __('Youtube URL'),
                'title' => __('Youtube URL'),
                'required' => true,
                'style' => 'width:500px; height:200px;',
                'wysiwyg' => true,
                'class' => 'validate-url',
                'comment'=> 'Example: https://www.youtube.com/embed/c0mX-5q3mrY'
            ]
        );

        $fieldset->addField(
            'video_status',
            'select',
            [   'name' => 'video_status',
                'label' => __('Enable Video Press'),
                'title' => __('Enable Video Press'),
                'required' => false,
                'values'=> array('0' => 'Disable', '1'=> 'Enable'),
            ]
        );

        $fieldset->addField(
            'video_widget',
            'select',
            [
                'name' => 'video_widget',
                'label' => __('Show in Widget Slider'),
                'title' => __('Show in Widget Slider'),
                'required' => false,
                'values' => array(
                    '0' => 'Not Show In Widget',
                    '1' => 'Show In Widget'
                )

            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Video Press Information Tab');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Video Press Information Tab');
    }

    /**
     * Prepare can show tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Prepare is hidden tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }


}