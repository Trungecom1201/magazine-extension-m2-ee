<?php

namespace Plumtreegroup\Magazine\Block\Adminhtml\Videomagazine\Edit;


class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('video_magazine_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Video Press Tabs'));
    }

}