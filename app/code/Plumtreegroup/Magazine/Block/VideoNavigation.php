<?php
namespace Plumtreegroup\Magazine\Block;
class VideoNavigation extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Plumtreegroup\Magazine\Helper\Data
     */
    protected $_helperMagazine;
    /**
     * @var
     */
    protected $_resources;
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory
     */
    protected $_videoMagazineCollection;
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory
     */
    protected $_typeCollection;
    /**
     * @var \Magento\Directory\Block\Data
     */
    protected $_directoryBlock ;

    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\Collection
     */
    protected $_magazineModel;

    /**
     * VideoNavigation constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Plumtreegroup\Magazine\Helper\Data $helperMagazine
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory $magazineCollection
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\Collection $magazineModel
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory $typeCollection
     * @param \Magento\Directory\Block\Data $directoryBlock
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumtreegroup\Magazine\Helper\Data $helperMagazine,
        \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory $videoMagazineCollection,
        \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\Collection $magazineModel,
        \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory $typeCollection,
        \Magento\Directory\Block\Data $directoryBlock,array $data = [])
    {
        $this->_helperMagazine = $helperMagazine;
        $this->_videoMagazineCollection = $videoMagazineCollection;
        $this->_magazineModel = $magazineModel;
        $this->_typeCollection = $typeCollection;
        $this->_directoryBlock = $directoryBlock;
        parent::__construct($context);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * @return \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\CollectionFactory
     */
    public function getAllMagazine(){
        $Collection = $this->_videoMagazineCollection->create();
        return $Collection;
    }

    /**
     * @return column video_type_magazine only
     */
    public function getValuesTypeMagazine(){
        $collection = $this->getAllMagazine();
        return $collection->getColumnValues('video_type_id');
    }

    /**
     * @return column video_type_magazine only
     */
    public function getValuesNameVideoMagazine(){
        $collection = $this->getAllMagazine();
        return $collection->getColumnValues('video_name');
    }

    /**
     * @return column id_type only
     */
    public function getListType(){
        $listId = $this->getValuesTypeMagazine();

        return array_unique($listId);
    }
    /**
     * @return column products only
     */
    public function getProductAssignMagazine(){
        $collection = $this->getAllMagazine();
        return $collection->getColumnValues('products');
    }

    /**
     * @return array list product
     */
    public function getListProduct(){
        $productIds = $this->getProductAssignMagazine();
        $result = "";
        foreach ($productIds as $productId){
            $result .= $productId.',';
        }
        $idpro = explode(',',$result);
        return array_unique($idpro);
    }

    /**
     * @param page
     * @return numberitem of page
     */
    public function getParamItem(){
        $params = $this->getParam();
        return $params['page'] * 12;
    }

    /**
     * @param $id
     * @return \Plumtreegroup\Magazine\Helper\product_name
     */
    public function getProductById($id){
        $productCollection = $this->_helperMagazine->getNameProductById($id);
        return $productCollection;
    }

    /**
     * @param $typeId
     * @return type collection
     */
    public function getNameTypeVideo($typeId){
        $nameVideoCollection = $this->_helperMagazine->getNameTypeVideo($typeId);
        return $nameVideoCollection;
    }

    public function getParam(){
        $params = $this->getRequest()->getParams();
        return $params;
    }

    /**
     * @return array all year of magazine
     */
    public function getYearMagazine(){
        return $this->_magazineModel->getAllYearMagazine();
    }


}