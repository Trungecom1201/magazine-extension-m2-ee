<?php

namespace Plumtreegroup\Magazine\Block;
class Magazine extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Directory\Block\Data
     */
    protected $_directoryBlock ;

    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory
     */
    protected $_magazineModel ;

    /**
     * @var \Plumtreegroup\Magazine\Helper\Data
     */
    protected $_helperMagazine;

    /**
     * Magazine constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Plumtreegroup\Magazine\Helper\Data $helper
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory $magazineModel
     * @param \Magento\Directory\Block\Data $directoryBlock
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumtreegroup\Magazine\Helper\Data $helper,
        \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\CollectionFactory $magazineModel,
        \Magento\Directory\Block\Data $directoryBlock,array $data = [])
    {
        $this->_magazineModel = $magazineModel;
        $this->_directoryBlock = $directoryBlock;
        $this->_helperMagazine = $helper;
        parent::__construct($context);
    }

    public function getNumberMagazineCanShow(){
        $number = $this->_helperMagazine->getNumberItemCanShow();
        if(isset($number)){
            return $number;
        }else{
            return '12';
        }
    }
    /**
     * @return \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\Collection
     */
    public function getAllNumberMagazine(){
        $Collection = $this->_magazineModel->create();
        $Collection->setOrder('post_date','DESC');
        $Collection->setCurPage('1');
        $Collection->setPageSize($this->getNumberMagazineCanShow());
        $Collection->addFieldToFilter('magazine_status', '1');
        return $Collection;
    }

    public function getProductById($id){
        $productCollection = $this->_helperMagazine->getProductByEntityId($id);
        return $productCollection;
    }
    public function getNameTypeMagazine($typeId){
        $productCollection = $this->_helperMagazine->getNameTypeMagazine($typeId);
        return $productCollection;
    }

    public function getYear($year){
        $yearFormat = $this->_helperMagazine->getFormatDateMagazineToYear($year);
        return $yearFormat;
    }

    public function getMonth($month){
        $monthFormat = $this->_helperMagazine->getFormatDateMagazineToMonth($month);
        return $monthFormat;
    }

    public function getMediapath($nameImage){
        return $this->_helperMagazine->getImageUrl($nameImage);
    }

    public function getParam(){
        $params = $this->getRequest()->getParams();
        return $params;
    }
    public function getParamItem(){
        $params = $this->getRequest()->getParams();
        if(isset($params['page'])){
            return $params['page'];
        }else{
            return '1';
        }

    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}