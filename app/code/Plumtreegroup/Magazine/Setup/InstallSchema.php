<?php

namespace Plumtreegroup\Magazine\Setup;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $magazineTypeTable = $installer->getConnection()
            ->newTable($installer->getTable('cus_type_magazine'))
            ->addColumn('id_type',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true])

            ->addColumn('type_name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false])

            ->addColumn('created_at',
                Table::TYPE_DATETIME,
                255,
                ['nullable' => true])

            ->addColumn('updated_at',
                Table::TYPE_DATETIME,
                255,
                ['nullable' => true]);

        $installer->getConnection()->createTable($magazineTypeTable);

        $NumberMagazineTable = $installer->getConnection()
            ->newTable($installer->getTable('magazine_number'))
            ->addColumn('id_magazine_number',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true])

            ->addColumn('id_type',
                Table::TYPE_INTEGER,
                11,
                ['nullable' => false])

            ->addColumn('products',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false])

            ->addColumn('name_number',
                Table::TYPE_TEXT,
                100,
                ['nullable' => true])

            ->addColumn('url_magazine',
                Table::TYPE_TEXT,
                100,
                ['nullable' => true])

            ->addColumn('image',
                Table::TYPE_TEXT,
                500,
                ['nullable' => true])

            ->addColumn('description',
                Table::TYPE_TEXT,
                500,
                ['nullable' => true])

            ->addColumn('created_at',
                Table::TYPE_DATETIME,
                255,
                ['nullable' => true])

            ->addColumn('updated_at',
                Table::TYPE_DATETIME,
                255,
                ['nullable' => true])
            ;



        $installer->getConnection()->createTable($NumberMagazineTable);

        $installer->endSetup();
    }
}