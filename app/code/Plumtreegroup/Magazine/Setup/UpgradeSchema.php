<?php

namespace Plumtreegroup\Magazine\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context)
    {
        $installer = $setup;
        $setup->startSetup();
        if (version_compare($context->getVersion(), '0.1.0') < 0) {
            $tableName = $setup->getTable('magazine_number');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 500,
                    'nullable' => false,
                    'comment' => 'Big Image',
                    'default' => ''
                ];
                $installer->getConnection()->addColumn($tableName, 'big_image', $column);
            }
        }
        /**
         *  version 0.1.1  add table Video
         */
        if (version_compare($context->getVersion(), '0.1.1') < 0) {
            $magazineVideoTable = $installer->getConnection()
                ->newTable($installer->getTable('video_magazine'))
                ->addColumn('video_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'nullable' => false, 'primary' => true])
                ->addColumn('video_name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false])
                ->addColumn('video_url',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false])
                ->addColumn('video_type',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false])
                ->addColumn('created_at',
                    Table::TYPE_DATETIME,
                    255,
                    ['nullable' => true])
                ->addColumn('updated_at',
                    Table::TYPE_DATETIME,
                    255,
                    ['nullable' => true]);

            $installer->getConnection()->createTable($magazineVideoTable);

            $magazineVideoTypeTable = $installer->getConnection()
                ->newTable($installer->getTable('video_type_magazine'))
                ->addColumn('video_type_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'nullable' => false, 'primary' => true])
                ->addColumn('video_type_name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false])
                ->addColumn('created_at',
                    Table::TYPE_DATETIME,
                    255,
                    ['nullable' => true])
                ->addColumn('updated_at',
                    Table::TYPE_DATETIME,
                    255,
                    ['nullable' => true]);

            $installer->getConnection()->createTable($magazineVideoTypeTable);
        }
        /**
         *  version 0.1.1  add table Video
         */
        if (version_compare($context->getVersion(), '0.1.2') < 0) {
            $tableName = $setup->getTable('video_magazine');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => false,
                    'comment' => 'product',
                    'default' => ''
                ];
                $installer->getConnection()->addColumn($tableName, 'products', $column);
            }
        }

        /**
         *  version 0.1.3  change name column
         */
        if (version_compare($context->getVersion(), '0.1.3') < 0) {
            $tableName = $setup->getTable('video_magazine');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $editOrderTable = $installer->getConnection();
                $editOrderTable
                    ->changeColumn(
                        $tableName,
                        'video_type',
                        'video_type_id',
                        ['type' => Table::TYPE_TEXT, 255, 'nullable' => false],
                        'Video type');
            }
        }

        /**
         *  version 0.1.4  add column image video
         */
        if (version_compare($context->getVersion(), '0.1.4') < 0) {
            $tableName = $setup->getTable('video_magazine');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 500,
                    'nullable' => false,
                    'comment' => 'image_video',
                    'default' => ''
                ];
                $installer->getConnection()->addColumn($tableName, 'image_video', $column);
            }
        }

        /**
         *  version 0.1.5  update field backend
         */
        if (version_compare($context->getVersion(), '0.1.5') < 0) {
            $tableVideoName = $setup->getTable('video_magazine');
            $tableMagazineName = $setup->getTable('magazine_number');
            if ($setup->getConnection()->isTableExists($tableVideoName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 50,
                    'nullable' => false,
                    'comment' => 'Store view',
                    'default' => ''
                ];
                $installer->getConnection()->addColumn($tableVideoName, 'store_ids', $column);
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 11,
                    'nullable' => false,
                    'comment' => 'Status Video',
                    'default' => '0'
                ];
                $installer->getConnection()->addColumn($tableVideoName, 'video_status', $column);
            }
            if ($setup->getConnection()->isTableExists($tableMagazineName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 50,
                    'nullable' => false,
                    'comment' => 'Store view',
                    'default' => ''
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'store_ids', $column);
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 11,
                    'nullable' => false,
                    'comment' => 'Status Video',
                    'default' => '0'
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'magazine_status', $column);
            }
        }

        /**
         *  version 0.1.6 update field backend
         */
        if (version_compare($context->getVersion(), '0.1.6') < 0) {
            $tableVideoName = $setup->getTable('video_magazine');
            $tableMagazineName = $setup->getTable('magazine_number');
            if ($setup->getConnection()->isTableExists($tableMagazineName) == true) {
                $column = [
                    'type' => Table::TYPE_DATETIME,
                    'length' => 255,
                    'nullable' => false,
                    'comment' => 'Post Date',
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'post_date', $column);
            }
            if ($setup->getConnection()->isTableExists($tableVideoName) == true) {
                $column = [
                    'type' => Table::TYPE_DATETIME,
                    'length' => 255,
                    'nullable' => false,
                    'comment' => 'Post Date',
                ];
                $installer->getConnection()->addColumn($tableVideoName, 'post_date', $column);
            }
        }

        /**
         *  version 0.1.7 update field backend
         */
        if (version_compare($context->getVersion(), '0.1.7') < 0) {
            $tableMagazineName = $setup->getTable('magazine_number');
            if ($setup->getConnection()->isTableExists($tableMagazineName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 50,
                    'nullable' => false,
                    'comment' => 'Product Line',
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'product_line', $column);
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 11,
                    'nullable' => false,
                    'comment' => 'Position',
                    'default' => '0'
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'position', $column);
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => false,
                    'comment' => 'Subtitle',
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'subtitle', $column);
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => false,
                    'comment' => 'Hover Title',
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'hover_title', $column);
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => false,
                    'comment' => 'Hover Subtitle',
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'hover_subtitle', $column);
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 500,
                    'nullable' => false,
                    'comment' => 'Change button  url title',
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'change_button_url_title', $column);
            }
        }

        /**
         *  version 0.1.8 update field backend
         */
        if (version_compare($context->getVersion(), '0.1.8') < 0) {
            $tableVideoName = $setup->getTable('video_magazine');
            $tableMagazineName = $setup->getTable('magazine_number');
            if ($setup->getConnection()->isTableExists($tableMagazineName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 500,
                    'nullable' => false,
                    'comment' => 'Rendor On',
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'rendor_on', $column);
            }

            if ($setup->getConnection()->isTableExists($tableVideoName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 11,
                    'nullable' => false,
                    'comment' => 'Position',
                    'default' => '0'
                ];
                $installer->getConnection()->addColumn($tableVideoName, 'position', $column);
            }
        }
        /**
         *  version 0.1.9 update field backend
         */
        if (version_compare($context->getVersion(), '0.1.9') < 0) {

            $tableMagazineName = $setup->getTable('magazine_number');
            if ($setup->getConnection()->isTableExists($tableMagazineName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 500,
                    'nullable' => false,
                    'comment' => 'Category',
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'magazine_category', $column);
            }
        }
        /**
         *  version 0.2.0 update field backend
         */
        if (version_compare($context->getVersion(), '0.2.0') < 0) {

            $tableMagazineName = $setup->getTable('magazine_number');
            $tableVideoName = $setup->getTable('video_magazine');
            if ($setup->getConnection()->isTableExists($tableMagazineName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 500,
                    'nullable' => false,
                    'comment' => 'Magazine show widget',
                ];
                $installer->getConnection()->addColumn($tableMagazineName, 'magazine_widget', $column);
            }
            if ($setup->getConnection()->isTableExists($tableVideoName) == true) {
                $column = [
                    'type' => Table::TYPE_TEXT,
                    'length' => 500,
                    'nullable' => false,
                    'comment' => 'Video show widget',
                ];
                $installer->getConnection()->addColumn($tableVideoName, 'video_widget', $column);
            }
        }
        /**
         *  version 0.2.1 update field backend
         */
        if (version_compare($context->getVersion(), '0.2.1') < 0) {
            $tableVideoName = $setup->getTable('video_magazine');
            if ($setup->getConnection()->isTableExists($tableVideoName) == true) {
                $editOrderTable = $installer->getConnection();
                $editOrderTable
                    ->changeColumn(
                        $tableVideoName,
                        'image_video',
                        'image',
                        ['type' => Table::TYPE_TEXT, 255, 'nullable' => false],
                        'Video type');
            }
        }
        $setup->endSetup();
    }
}