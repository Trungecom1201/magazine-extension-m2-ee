<?php
namespace Plumtreegroup\Magazine\Controller;

/**
 * Inchoo Custom router Controller Router
 *
 * @author      Zoran Salamun <zoran.salamun@inchoo.net>
 */
class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    /**
     * Response
     *
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @var \Plumtreegroup\Magazine\Helper\Data
     */
    protected $_helperFunction;

    /**
     * Router constructor.
     * @param \Plumtreegroup\Magazine\Helper\Data $helperFunction
     * @param \Magento\Framework\App\ActionFactory $actionFactory
     * @param \Magento\Framework\App\ResponseInterface $response
     */
    public function __construct(
        \Plumtreegroup\Magazine\Helper\Data $helperFunction,
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magento\Framework\App\ResponseInterface $response
    ) {
        $this->_helperFunction = $helperFunction;
        $this->actionFactory = $actionFactory;
        $this->_response = $response;
    }

    /**
     * Validate and Match
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {

        $identifier = trim($request->getPathInfo(), '/');
        if(strpos($identifier, $this->_helperFunction->getFrontNameVideo()) !== false) {

            $request->setModuleName('magazine')->setControllerName('page')->setActionName('video');
        } else if(strpos($identifier, $this->_helperFunction->getFrontNameMagazine()) !== false) {

            $request->setModuleName('magazine')->setControllerName('page')->setActionName('magazine');
        } else {
            return;
        }


        return $this->actionFactory->create(
            'Magento\Framework\App\Action\Forward',
            ['request' => $request]
        );
    }
}
