<?php
namespace Plumtreegroup\Magazine\Controller\Page;

use Magento\Framework\View\Result\PageFactory;
class getFilterVideo extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\Collection
     */
    protected $_videoMagazineCollection;

    /**
     * @var \Plumtreegroup\Magazine\Helper\Data
     */
    protected $_helperFunction;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_coreHelper;

    /**
     * getFilterMagazineByProduct constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param PageFactory $resultPageFactory
     * @param \Plumtreegroup\Magazine\Helper\Data $helper
     * @param \Magento\Framework\Json\Helper\Data $coreHelper
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\Collection $videoMagazineCollection
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        PageFactory $resultPageFactory,
        \Plumtreegroup\Magazine\Helper\Data $helper,
        \Magento\Framework\Json\Helper\Data $coreHelper,
        \Plumtreegroup\Magazine\Model\ResourceModel\Videomagazine\Collection $videoMagazineCollection
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_coreHelper = $coreHelper;
        $this->_videoMagazineCollection = $videoMagazineCollection;
        $this->_helperFunction = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__(' heading '));
        $block = $resultPage->getLayout()
            ->createBlock('Plumtreegroup\Magazine\Block\FilterVideoMagazine')
            ->setTemplate('Plumtreegroup_Magazine::page/video/result.phtml')->toHtml();

        return $this->getResponse()->representJson($this->_coreHelper->jsonEncode(['success'=>true,'html' => $block])
        );
    }
}