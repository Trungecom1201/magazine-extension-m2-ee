<?php
namespace Plumtreegroup\Magazine\Controller\Page;

use Magento\Framework\View\Result\PageFactory;
class getFilterMagazineByProduct extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\Collection
     */
    protected $_magazineCollection;

    /**
     * @var \Plumtreegroup\Magazine\Helper\Data
     */
    protected $_helperFunction;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_coreHelper;

    /**
     * getFilterMagazineByProduct constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param PageFactory $resultPageFactory
     * @param \Plumtreegroup\Magazine\Helper\Data $helper
     * @param \Magento\Framework\Json\Helper\Data $coreHelper
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\Collection $magazineCollection
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        PageFactory $resultPageFactory,
        \Plumtreegroup\Magazine\Helper\Data $helper,
        \Magento\Framework\Json\Helper\Data $coreHelper,
        \Plumtreegroup\Magazine\Model\ResourceModel\Numbermagazine\Collection $magazineCollection
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_coreHelper = $coreHelper;
        $this->_magazineCollection = $magazineCollection;
        $this->_helperFunction = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__(' heading '));
        $block = $resultPage->getLayout()
        ->createBlock('Plumtreegroup\Magazine\Block\FilterMagazine')
        ->setTemplate('Plumtreegroup_Magazine::page/magazine/result.phtml')->toHtml();

        return $this->getResponse()->representJson($this->_coreHelper->jsonEncode(['success'=>true,'html' => $block])
        );
    }
}