<?php

namespace Plumtreegroup\Magazine\Controller\Adminhtml\Typemagazine;

use Magento\Backend\App\Action\Context;
use Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

class MassDelete extends Action
{

    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $_filter;
    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * MassDelete constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        CollectionFactory $collectionFactory
    )
    {
        $this->_filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $collection = $this->_filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $item) {

            $item->delete();
        }

        $this->messageManager->addSuccess(__('A total of %1 Type Magazine(s) have been deleted.', $collectionSize));

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}