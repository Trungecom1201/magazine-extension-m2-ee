<?php

namespace Plumtreegroup\Magazine\Controller\Adminhtml\Typemagazine;

use Magento\Backend\App\Action;

class Save extends Action {

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date)
    {
        $this->date = $date;
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Plumtreegroup_Magazine::type_magazine_save');
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        $date = $this->date->gmtDate();

        if ($data) {
            $model = $this->_objectManager->create('Plumtreegroup\Magazine\Model\Typemagazine');

            $id = $this->getRequest()->getParam('id_type');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            try {
                if ($id) {
                    $model->setData('updated_at', $date);
                }else{
                    $model->setData('created_at', $date);
                }
                $model->save();
                $this->messageManager->addSuccess(__('You saved this type magazine.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id_type' => $model->getIdType(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError($e, __('Something went wrong while saving item.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id_type' => $this->getRequest()->getParam('id_type')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}