<?php

namespace Plumtreegroup\Magazine\Controller\Adminhtml\Typemagazine;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

class Index extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Plumtreegroup_Magazine::type_magazine_content');
        $resultPage->addBreadcrumb(__('Type Press'), __('Type Press'));
        $resultPage->addBreadcrumb(__('Manage Press'), __('Manage Type Press'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Type Press'));
        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Plumtreegroup_Magazine::type_magazine_content');
    }
}