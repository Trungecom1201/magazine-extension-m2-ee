<?php

namespace Plumtreegroup\Magazine\Controller\Adminhtml\Numbermagazine;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Plumtreegroup_Magazine::number_magazine_content');
        $resultPage->addBreadcrumb(__('Press'), __('Press'));
        $resultPage->addBreadcrumb(__('Manage Press'), __('Manage Press'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Press'));
        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Plumtreegroup_Magazine::number_magazine_content');
    }
}