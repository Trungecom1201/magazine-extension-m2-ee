<?php

namespace Plumtreegroup\Magazine\Controller\Adminhtml\Numbermagazine;

use Magento\Backend\App\Action;

class Delete extends Action {

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Plumtreegroup_Magazine::number_magazine_delete');
    }


    public function execute()
    {
        $id = $this->getRequest()->getParam('id_magazine_number');
        $resultRedirect = $this->resultRedirectFactory->create();

        if($id) {
            try {
                $model = $this->_objectManager->create('Plumtreegroup\Magazine\Model\Numbermagazine');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('This Magazine has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id_magazine_number' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a item to delete'));
        return $resultRedirect->setPath('*/*/');
    }
}