<?php
namespace Plumtreegroup\Magazine\Controller\Adminhtml\Numbermagazine;

class Save extends \Magento\Backend\App\Action {

    const MEDIA_PATH    = 'magazine/';
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Plumtreegroup\Magazine\Helper\Data
     */
    protected $_helperFunction;
    /**
     * @var
     */
    protected $_model;

    /**
     * Save constructor.
     * @param \Plumtreegroup\Magazine\Helper\Data $helperFunction
     * @param \Magento\Framework\Event\Manager $eventManager
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Plumtreegroup\Magazine\Model\Numbermagazine $model
     */
    public function __construct(
        \Plumtreegroup\Magazine\Helper\Data $helperFunction,
        \Magento\Framework\Event\Manager $eventManager,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Plumtreegroup\Magazine\Model\Numbermagazine $model
    )
    {
        $this->_helperFunction = $helperFunction;
        $this->_model = $model;
        $this->date = $date;
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Plumtreegroup_Magazine::number_magazine_save');
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $eventData = null;
        $resultRedirect = $this->resultRedirectFactory->create();
        $date = $this->date->gmtDate();
        if ($data) {
            $model = $this->_objectManager->create('Plumtreegroup\Magazine\Model\Numbermagazine');

            $id = $this->getRequest()->getParam('id_magazine_number');
            if ($id) {
                $model->load($id);
            }
            if (isset($data['big_image'])) {
                $bigImageData = $data['big_image'];
                unset($data['big_image']);
            } else {
                $bigImageData = array();
            }
            if (isset($data['image'])) {
                $imageData = $data['image'];
                unset($data['image']);
            } else {
                $imageData = array();
            }

            if(isset($data['stores'])) {
                if(in_array('0',$data['stores'])){
                    $data['store_ids'] = '0';
                }
                else{
                    $data['store_ids'] = implode(",", $data['stores']);
                }
                unset($data['stores']);
            }
            $model->setData($data);
            if(isset($data['products'])){
                $productIds =  str_replace('&',',',$data['products']);
                $model->setData('products' , $productIds);
            }
            try {
                /**
                 *  upload big image
                 */

                $bigImageFile = $this->_helperFunction->uploadImage('big_image');
                $path = $bigImageFile;

                if ($bigImageFile) {
                    $model->setBigImage($path);
                }

                if (isset($bigImageData['delete'])) {
                    $this->_helperFunction->removeImage($bigImageFile);
                    $model->setData('big_image','');
                }
                /**
                 * Upload image
                 */

                $imageFile = $this->_helperFunction->uploadImage('image');
                $pathimage = $imageFile;
                if ($imageFile) {
                    $model->setImage($pathimage);
                }

                if (isset($imageData['delete'])) {
                    $this->_helperFunction->removeImage($imageFile);
                    $model->setData('image','');
                }
                /**
                 *  set current date to data
                 */
                if ($id) {
                    $model->setData('updated_at', $date);
                }else{
                    $model->setData('created_at', $date);
                }

                $model->save();
                $this->messageManager->addSuccess(__('You saved this Magazine.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id_magazine_number' => $model->getIdMagazineNumber(), '_current' => true]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError($e, __('Something went wrong while saving item.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id_magazine_number' => $this->getRequest()->getParam('id_magazine_number')]);
        }

        return $resultRedirect->setPath('*/*/');
    }

}