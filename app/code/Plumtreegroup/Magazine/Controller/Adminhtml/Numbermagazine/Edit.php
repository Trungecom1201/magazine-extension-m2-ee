<?php

namespace Plumtreegroup\Magazine\Controller\Adminhtml\Numbermagazine;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;

class Edit extends Action
{

    protected $_coreRegistry = null;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Plumtreegroup_Magazine::number_magazine_save');
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Plumtreegroup_Magazine::number_magazine_content')
            ->addBreadcrumb(__('Press'), __('Press'))
            ->addBreadcrumb(__('Manage Press'), __('Manage Press'));
        return $resultPage;
    }

    public function execute()
    {

        $id = $this->getRequest()->getParam('id_magazine_number');
        $model = $this->_objectManager->create('Plumtreegroup\Magazine\Model\Numbermagazine');

        if ($id) {

            $model->load($id);
            if (!$model->getIdMagazineNumber()) {
                $this->messageManager->addError(__('This item no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }
        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $this->_coreRegistry->register('number_magazine_data', $model);
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Item') : __('New Press'),
            $id ? __('Edit Item') : __('New Press')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Press'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getTitle() : __('New Press'));

        return $resultPage;
    }
}