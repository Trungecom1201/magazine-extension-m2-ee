<?php

namespace Plumtreegroup\Magazine\Controller\Adminhtml\Videomagazine;

use Magento\Backend\App\Action;

class Save extends Action {

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Plumtreegroup\Magazine\Helper\Data
     */
    protected $_helperFunction;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param \Plumtreegroup\Magazine\Helper\Data $helperFunction
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        Action\Context $context,
        \Plumtreegroup\Magazine\Helper\Data $helperFunction,
        \Magento\Framework\Stdlib\DateTime\DateTime $date)
    {
        $this->_helperFunction = $helperFunction;
        $this->date = $date;
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Plumtreegroup_Magazine::video_magazine_save');
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        $date = $this->date->gmtDate();

        if ($data) {
            $model = $this->_objectManager->create('Plumtreegroup\Magazine\Model\Videomagazine');

            $id = $this->getRequest()->getParam('video_id');
            if ($id) {
                $model->load($id);
            }
            if (isset($data['image'])) {
                $imageData = $data['image'];
                unset($data['image']);
            } else {
                $imageData = array();
            }

            if(isset($data['stores'])) {
                if(in_array('0',$data['stores'])){
                    $data['store_ids'] = '0';
                }
                else{
                    $data['store_ids'] = implode(",", $data['stores']);
                }
                unset($data['stores']);
            }
            $model->setData($data);

            try {
                if(isset($data['products'])){
                    $productIds =  str_replace('&',',',$data['products']);
                    $model->setData('products' , $productIds);
                }
                if ($id) {
                    $model->setData('updated_at', $date);
                }else{
                    $model->setData('created_at', $date);
                }

                $imageFile = $this->_helperFunction->uploadImage('image');
                $pathimage = $imageFile;
                if ($imageFile) {
                    $model->setImage($pathimage);
                }

                if (isset($imageData['delete'])) {
                    $this->_helperFunction->removeImage($imageFile);
                    $model->setData('image','');
                }

                $model->save();
                $this->messageManager->addSuccess(__('You saved this Video Magazine.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['video_id' => $model->getVideoId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError($e, __('Something went wrong while saving item.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['video_id' => $this->getRequest()->getParam('video_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}