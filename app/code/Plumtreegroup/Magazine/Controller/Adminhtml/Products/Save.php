<?php

namespace Plumtreegroup\Magazine\Controller\Adminhtml\Products;

use Magento\Backend\App\Action;
use Magento\Catalog\Controller\Adminhtml\Product;

class Save extends \Magento\Catalog\Controller\Adminhtml\Product\Save

{

    /**
     * @var \Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper
     */
    protected $initializationHelper;


    /**
     * @var \Magento\Catalog\Model\Product\Copier
     */
    protected $productCopier;


    /**
     * @var \Magento\Catalog\Model\Product\TypeTransitionManager
     */
    protected $productTypeManager;


    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var \Magento\Authorization\Model\Acl\AclRetriever
     */
    protected $aclRetriever;


    private $storeManager;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param Product\Builder $productBuilder
     * @param Product\Initialization\Helper $initializationHelper
     * @param \Magento\Catalog\Model\Product\Copier $productCopier
     * @param \Magento\Authorization\Model\Acl\AclRetriever $aclRetriever
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Catalog\Model\Product\TypeTransitionManager $productTypeManager
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Action\Context $context,
        Product\Builder $productBuilder,
        Product\Initialization\Helper $initializationHelper,
        \Magento\Catalog\Model\Product\Copier $productCopier,
        \Magento\Authorization\Model\Acl\AclRetriever $aclRetriever,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Catalog\Model\Product\TypeTransitionManager $productTypeManager,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    )
    {
        parent::__construct($context, $productBuilder, $initializationHelper, $productCopier, $productTypeManager, $productRepository);
        $this->aclRetriever = $aclRetriever;
        $this->authSession = $authSession;
    }


    public function execute()
    {
        $storeId = $this->getRequest()->getParam('store', 0);
        $store = $this->getStoreManager()->getStore($storeId);
        $this->getStoreManager()->setCurrentStore($store->getCode());
        $redirectBack = $this->getRequest()->getParam('back', false);
        $productId = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $productAttributeSetId = $this->getRequest()->getParam('set');
        $productTypeId = $this->getRequest()->getParam('type');
        if ($data) {
            try {
                $product = $this->initializationHelper->initialize(
                    $this->productBuilder->build($this->getRequest())
                );
                $this->productTypeManager->processProduct($product);

                if (isset($data['product'][$product->getIdFieldName()])) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Unable to save product'));
                }

                $originalSku = $product->getSku();
                $product->save();
                $this->handleImageRemoveError($data, $product->getId());
                $this->getCategoryLinkManagement()->assignProductToCategories(
                    $product->getSku(),
                    $product->getCategoryIds()
                );
                $productId = $product->getEntityId();

                $productAttributeSetId = $product->getAttributeSetId();
                $productTypeId = $product->getTypeId();

                $this->copyToStores($data, $productId);

                $this->messageManager->addSuccess(__('You saved the product.'));
                $this->getDataPersistor()->clear('catalog_product');
                if ($product->getSku() != $originalSku) {
                    $this->messageManager->addNotice(
                        __(
                            'SKU for product %1 has been changed to %2.',
                            $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($product->getName()),
                            $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($product->getSku())
                        )
                    );
                }
                $this->_eventManager->dispatch(
                    'controller_action_catalog_product_save_entity_after',
                    ['controller' => $this, 'product' => $product]
                );

                if ($redirectBack === 'duplicate') {
                    $newProduct = $this->productCopier->copy($product);
                    $this->messageManager->addSuccess(__('You duplicated the product.'));
                }
                /**
                 * add assign product to magazine
                 */
                if ($this->getRequest()->getParam('magazine_products') && $productId) {
                    $this->addProductToMagazine((array)json_decode($this->getRequest()->getParam('magazine_products')),$productId);
                }
                if ($this->getRequest()->getParam('video_products') && $productId) {
                    $this->addProductToVideoMagazine((array)json_decode($this->getRequest()->getParam('video_products')),$productId);
                }

            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
                $this->getDataPersistor()->set('catalog_product', $data);
                $redirectBack = $productId ? true : 'new';
            } catch (\Exception $e) {
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->messageManager->addError($e->getMessage());
                $this->getDataPersistor()->set('catalog_product', $data);
                $redirectBack = $productId ? true : 'new';
            }
        } else {
            $resultRedirect->setPath('catalog/*/', ['store' => $storeId]);
            $this->messageManager->addError('No data to save');
            return $resultRedirect;
        }

        if ($redirectBack === 'new') {
            $resultRedirect->setPath(
                'catalog/*/new',
                ['set' => $productAttributeSetId, 'type' => $productTypeId]
            );
        } elseif ($redirectBack === 'duplicate' && isset($newProduct)) {
            $resultRedirect->setPath(
                'catalog/*/edit',
                ['id' => $newProduct->getEntityId(), 'back' => null, '_current' => true]
            );
        } elseif ($redirectBack) {
            $resultRedirect->setPath(
                'catalog/*/edit',
                ['id' => $productId, '_current' => true, 'set' => $productAttributeSetId]
            );
        } else {
            $resultRedirect->setPath('catalog/*/', ['store' => $storeId]);
        }
        return $resultRedirect;
    }

    private function getStoreManager()
    {
        if (null === $this->storeManager) {
            $this->storeManager = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Store\Model\StoreManagerInterface');
        }
        return $this->storeManager;
    }



    /**
     * Notify Plumtreegrouper when image was not deleted in specific case.
     * TODO: temporary workaround must be eliminated in MAGETWO-45306
     *
     * @param array $postData
     * @param int $productId
     * @return void
     */
    private function handleImageRemoveError($postData, $productId)
    {
        if (isset($postData['product']['media_gallery']['images'])) {
            $removedImagesAmount = 0;
            foreach ($postData['product']['media_gallery']['images'] as $image) {
                if (!empty($image['removed'])) {
                    $removedImagesAmount++;
                }
            }
            if ($removedImagesAmount) {
                $expectedImagesAmount = count($postData['product']['media_gallery']['images']) - $removedImagesAmount;
                $product = $this->productRepository->getById($productId);
                if ($expectedImagesAmount != count($product->getMediaGallery('images'))) {
                    $this->messageManager->addNotice(
                        __('The image cannot be removed as it has been assigned to the other image role')
                    );
                }
            }
        }
    }


    /**
     * @return \Magento\Catalog\Api\CategoryLinkManagementInterface
     */
    private function getCategoryLinkManagement()
    {
        if (null === $this->categoryLinkManagement) {
            $this->categoryLinkManagement = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Catalog\Api\CategoryLinkManagementInterface');
        }
        return $this->categoryLinkManagement;
    }


    /**
     * assign product to Magazine
     * @param $magazineId
     * @param $productId
     */
    public function addProductToMagazine($magazineId,$productId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $magazineModel =   $objectManager->get('\Plumtreegroup\Magazine\Model\Magazine');
        $magazineCollection =   $objectManager->get('\Plumtreegroup\Magazine\Model\Numbermagazine');

        $magazineInProduct = $magazineModel->getMagazineInCurrentProduct();
        foreach ($magazineInProduct as $magazine) {
            $productIds = $magazine->getProducts();
            $productArray = explode(',',$productIds);

            //check and remove product id in array
            if(($key = array_search($productId, $productArray)) !== false) {
                unset($productArray[$key]);
            }
            $magazine->setData('products',implode(',',$productArray))->save();
        }
        //add product id to magazine selected
        foreach($magazineId as $key => $val){
            $magazineCollection = $magazineCollection->load((int)$key);
            $productIds = $magazineCollection->getProducts();
            if ($productIds != null || $productIds != '') {
                $magazineCollection->setData('products',$productIds.','.$productId);
            } else {
                $magazineCollection->setData('products',$productId);
            }
            $magazineCollection->save();
        }
    }

    /**
     * assign product to Magazine
     * @param $videoId
     * @param $productId
     */
    public function addProductToVideoMagazine($videoId,$productId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $videoModel =   $objectManager->get('\Plumtreegroup\Magazine\Model\Video');
        $videoMagazineCollection =   $objectManager->get('\Plumtreegroup\Magazine\Model\Videomagazine');

        $videoMagazineInProduct = $videoModel->getVideoMagazineInCurrentProduct();
        foreach ($videoMagazineInProduct as $video) {
            $productIds = $video->getProducts();
            $productArray = explode(',',$productIds);

            //check and remove product id in array
            if(($key = array_search($productId, $productArray)) !== false) {
                unset($productArray[$key]);
            }
            $video->setData('products',implode(',',$productArray))->save();
        }
        //add product id to magazine selected
        foreach($videoId as $key => $val){
            $VideoMagazineCollection = $videoMagazineCollection->load((int)$key);
            $productIds = $VideoMagazineCollection->getProducts();
            if ($productIds != null || $productIds != '') {
                $VideoMagazineCollection->setData('products',$productIds.','.$productId);
            } else {
                $VideoMagazineCollection->setData('products',$productId);
            }
            $VideoMagazineCollection->save();
        }
    }
}
