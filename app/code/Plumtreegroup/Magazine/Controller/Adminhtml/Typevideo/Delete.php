<?php

namespace Plumtreegroup\Magazine\Controller\Adminhtml\Typevideo;

use Magento\Backend\App\Action;

class Delete extends Action {


    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Plumtreegroup_Magazine::type_video_delete');
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('video_type_id');
        $resultRedirect = $this->resultRedirectFactory->create();

        if($id) {
            try {
                $model = $this->_objectManager->create('Plumtreegroup\Magazine\Model\Typevideo');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('This Magazine has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['video_type_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a item to delete'));
        return $resultRedirect->setPath('*/*/');
    }
}