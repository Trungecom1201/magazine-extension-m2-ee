var config = {
    map: {
        '*': {
            custom: 'Plumtreegroup_Magazine/js/custom',
            fancybox: 'Plumtreegroup_Magazine/js/jquery.fancybox',
			owl :  'Plumtreegroup_Magazine/js/owl/owl.carousel'
        }
    },
    shim: {
        fancybox: {
            deps: ['jquery']
        },
        owl: {
            deps: ['jquery']
        }
    }
};