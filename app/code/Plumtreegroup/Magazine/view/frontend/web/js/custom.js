define([
    'jquery'
], function ($j) {
    $j(document).ready(function () {

        size_li = $j(".filter-options-content.year li").size();
        x = 3;
        if(4 <= size_li){
            $j('#showLessYear').hide();

            $j('.filter-options-content.year li:lt(' + x + ')').show();
            $j('#loadMoreYear').click(function () {
                x = (x + 5 <= size_li) ? x + 5 : size_li;
                $j('.filter-options-content.year li:lt(' + x + ')').show();
                $j('#showLessYear').show();
                if (x == size_li) {
                    $j('#loadMoreYear').hide();
                }
            });
            $j('#showLessYear').click(function () {
                x = (x - 5 < 0) ? 3 : x - 5;
                $j('.filter-options-content.year li').not(':lt(' + x + ')').hide();
                $j('#loadMoreYear').show();
                $j('#showLessYear').show();
                if (x <= 3) {
                    $j('#showLessYear').hide();
                }
            });
        }
        else {$j('#showLessYear').hide();$j('#loadMoreYear').hide(); $j('.filter-options-content.year li:lt(' + x + ')').show(); }

        size_type_li = $j(".filter-options-content.type li").size();
        x = 4;
        if(size_type_li >= 4){
            $j('#showLessType').hide();

            $j('.filter-options-content.type li:lt(' + x + ')').show();
            $j('#loadMoreType').click(function () {
                x = (x + 5 <= size_type_li) ? x + 5 : size_type_li;
                $j('.filter-options-content.type li:lt(' + x + ')').show();
                $j('#showLessType').show();
                if (x == size_type_li) {
                    $j('#loadMoreType').hide();
                }
            });
            $j('#showLessType').click(function () {
                x = (x - 5 < 0) ? 3 : x - 5;
                $j('.filter-options-content.type li').not(':lt(' + x + ')').hide();
                $j('#loadMoreType').show();
                $j('#showLessType').show();
                if (x <= 3) {
                    $j('#showLessType').hide();
                }
            });
        }else {$j('#showLessType').hide(); $j('#loadMoreType').hide(); $j('.filter-options-content.type li:lt(' + x + ')').show();}

        size_products_li = $j(".filter-options-content.products li").size();
        x = 5;
        if(size_products_li >= 5){
            $j('#showLessProducts').hide();

            $j('.filter-options-content.products li:lt(' + x + ')').show();
            $j('#loadMoreProducts').click(function () {
                x = (x + 5 <= size_products_li) ? x + 5 : size_products_li;
                $j('.filter-options-content.products li:lt(' + x + ')').show();
                $j('#showLessProducts').show();
                if (x == size_products_li) {
                    $j('#loadMoreProducts').hide();
                }
            });
            $j('#showLessProducts').click(function () {
                x = (x - 5 < 0) ? 3 : x - 5;
                $j('.filter-options-content.products li').not(':lt(' + x + ')').hide();
                $j('#loadMoreProducts').show();
                $j('#showLessProducts').show();
                if (x <= 5) {
                    $j('#showLessProducts').hide();
                }
            });
        }else {$j('#showLessProducts').hide();  $j('#loadMoreProducts').hide();  $j('.filter-options-content.products li:lt(' + x + ')').show();}

        size_products_li = $j(".filter-options-content.name li").size();
        x = 3;
        if(size_products_li >= 3){
            $j('#showLessName').hide();

            $j('.filter-options-content.name li:lt(' + x + ')').show();
            $j('#loadMoreName').click(function () {
                x = (x + 5 <= size_products_li) ? x + 5 : size_products_li;
                $j('.filter-options-content.name li:lt(' + x + ')').show();
                $j('#showLessName').show();
                if (x == size_products_li) {
                    $j('#loadMoreName').hide();
                }
            });
            $j('#showLessName').click(function () {
                x = (x - 5 < 0) ? 3 : x - 5;
                $j('.filter-options-content.name li').not(':lt(' + x + ')').hide();
                $j('#loadMoreName').show();
                $j('#showLessName').show();
                if (x <= 3) {
                    $j('#showLessName').hide();
                }
            });
        }else {$j('#showLessName').hide(); $j('#loadMoreName').hide(); $j('.filter-options-content.name li:lt(' + x + ')').show();}
    });
});
