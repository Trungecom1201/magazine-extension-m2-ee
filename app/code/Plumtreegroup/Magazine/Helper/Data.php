<?php

namespace Plumtreegroup\Magazine\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const MEDIA_PATH    = 'magazine/';
    const MAX_FILE_SIZE = 1048576;
    const XML_PATH_Number_Magazine = 'helloworld/general/custom_path';
    const XML_PATH_URL_Number_Magazine = 'helloworld/general/router_name';
    const XML_PATH_Number_Video_Magazine = 'video/general/custom_path';
    const XML_PATH_URL_Video_Magazine = 'video/general/router_name';

    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory
     */
    protected $_typeCollection;

    /**
     * @var \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory
     */
    protected $_typeVideoCollection;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepo;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productloader;

    /**
     * @var \Magento\Framework\HTTP\Adapter\FileTransferFactory
     */
    protected $httpFactory;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    protected $_ioFile;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    protected $mediaDirectory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;


    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Backend\Model\UrlInterface $backendUrl
     * @param \Magento\Catalog\Model\ProductFactory $productloader
     * @param \Magento\Catalog\Model\ProductRepository $productRepo
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory $typeCollection
     * @param \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory $typeCollection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filesystem\Io\File $ioFile
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Magento\Framework\HTTP\Adapter\FileTransferFactory $httpFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Catalog\Model\ProductFactory $productloader,
        \Magento\Catalog\Model\ProductRepository $productRepo,
        \Plumtreegroup\Magazine\Model\ResourceModel\Typemagazine\CollectionFactory $typeCollection,
        \Plumtreegroup\Magazine\Model\ResourceModel\Typevideo\CollectionFactory $typeVideoCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filesystem\Io\File $ioFile,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\HTTP\Adapter\FileTransferFactory $httpFactory

    ) {
        parent::__construct($context);
        $this->_backendUrl = $backendUrl;
        $this->scopeConfig = $scopeConfig;
        $this->_typeCollection = $typeCollection;
        $this->_typeVideoCollection = $typeVideoCollection;
        $this->_productRepo = $productRepo;
        $this->_productloader = $productloader;
        $this->storeManager = $storeManager;
        $this->httpFactory = $httpFactory;
        $this->_ioFile = $ioFile;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->filesystem = $filesystem;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
    }

    /**
     * get products tab Url in admin
     * @return string
     */
    public function getProductsGridUrl()
    {
        return $this->_backendUrl->getUrl('magazine/numbermagazine/products', ['_current' => true]);
    }
    public function getProductsGridVideoUrl()
    {
        return $this->_backendUrl->getUrl('magazine/videomagazine/products', ['_current' => true]);
    }


    /**
     * @param $nameImage
     * @return string
     */

    public function getImageUrl($nameImage){
            return $this->storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ) . $nameImage;
    }

    /**
     * @param $baseTime
     * @return false|string
     */
    public function getFormatDateMagazineToYear($baseTime){
        return date("Y", strtotime($baseTime));
    }

    /**
     * @param $baseTime
     * @return false|string
     */
    public function getFormatDateMagazineToMonth($baseTime){
        return date("F", strtotime($baseTime));
    }

    /**
     * @param $IdProduct
     * @return $this
     */
    public function getProductByEntityId($IdProduct){
        return $this->_productloader->create()->load($IdProduct);
    }

    /**
     * @param $idTypeMagazine
     * @return $dataTypeMagazine
     */
    public function getNameTypeMagazine($idTypeMagazine){
        $dataTypeMagazine = $this->_typeCollection->create()->addFieldToFilter('id_type', $idTypeMagazine);
        return $dataTypeMagazine;
    }


    public function getNameTypeVideo($idVideoTypeMagazine){
        $dataTypeVideo = $this->_typeVideoCollection->create()->addFieldToFilter('video_type_id', $idVideoTypeMagazine);
        return $dataTypeVideo;
    }

    /**
     * @param $productId
     * @return product_name
     */
    public function getNameProductById($productId){
        $productData = $this->_productRepo->getById($productId);
        return $productData->getName();
    }

    /**
     * @return string params
     */
    public function getParamsMagazine(){
        $params = $this->getRequest()->getParams();
        $paramFilter = $params['type'].','.$params['product'].','.$params['year'];
        return $paramFilter;
    }

    public function uploadImage($scope)
    {
        $adapter = $this->httpFactory->create();
        $adapter->addValidator(
            new \Zend_Validate_File_FilesSize(['max' => self::MAX_FILE_SIZE])
        );

        if ($adapter->isUploaded($scope)) {
            // validate image
            if (!$adapter->isValid($scope)) {
//                throw new \Magento\Framework\Model\Exception(__('Uploaded image is not valid.'));
            }

            $uploader = $this->_fileUploaderFactory->create(['fileId' => $scope]);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);
            $uploader->setAllowCreateFolders(true);

            if ($uploader->save($this->getBaseDir())) {
                return self::MEDIA_PATH.$uploader->getUploadedFileName();
            }
        }
        return false;
    }

    /**
     * @param $imageFile
     * @return bool
     */
    public function removeImage($imageFile)
    {
        $io = $this->_ioFile;
        $io->open(array('path' => $this->getBaseDir()));
        if ($io->fileExists($imageFile)) {
            return $io->rm($imageFile);
        }
        return false;
    }

    /**
     * @return string media path
     */
    public function getBaseDir()
    {
        $path = $this->filesystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath(self::MEDIA_PATH);

        return $path;
    }

    public function  getBaseUrl(){
        return $this->storeManager->getStore()->getBaseUrl();
    }

    public function getNumberItemCanShow() {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue(self::XML_PATH_Number_Magazine, $storeScope);


    }
    public function getNumberVideoCanShow() {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue(self::XML_PATH_Number_Video_Magazine, $storeScope);


    }
    public function getFrontNameMagazine() {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $urlName = $this->scopeConfig->getValue(self::XML_PATH_URL_Number_Magazine, $storeScope);
        if ($urlName){
            return $urlName ;
        }else{
            return 'press';
        }

    }
    public function getFrontNameVideo() {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $urlVideoName = $this->scopeConfig->getValue(self::XML_PATH_URL_Video_Magazine, $storeScope);
        if($urlVideoName){
            return $urlVideoName;
        }else{
            return 'video';
        }
    }
}


